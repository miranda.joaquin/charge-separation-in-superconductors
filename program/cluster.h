typedef struct  tagPARAM{
	int dim;
	double filling;
	int pottype;
	double g2;
	double anizo;
	double range;
	int init;
	double T;
	long nstartMC;
	long nMC;
	char ipar[64];
	double istart;
	double istop;
	double istep;
	char fname[1024];
	double mu;
	double T0; //parameters for adjusting MC steps - reference temperature
	long nMC0; //parameters for adjusting MC steps - MC steps at reference T  
	double Ea; //parameters for adjusting MC steps - activation energy
	int iflag; //if 1 iterate linearly inverse value of a parameter 
	double minstep; // used in conjunction with iflag
	int clflag; //if 1 count clusters
	int corrflag; //if 1 calc space correlation
	int condflag;

} PARAM;

typedef struct tagLUPFUNC{//??
	double minx;
	double maxx;
	long dim;
	double* py;
} LUPFUNC;

typedef struct tagLUPBA{
	double minx;
	double maxx;
	long dim;
	double* pb;
	double* pa;
	double* pg;
} LUPBA;



typedef struct tagPARTICLE{
	int i; //coordinates of the particle
	int j;
	char type;// Two types of particles: 0,1
} PARTICLE;

typedef struct  tagMODEL{
	int dim; //the dimension of the system
	long npart; //the number of particles in the system		/
	double g2; //the coupling constant
	double anizo; //the anizotropy
	double range; //the range of interaction
	int init; //how to initialize
	PARTICLE *p; //linear array of particles of size dim*dim 
	double T; //the tmperature
	double mu; //the chemical potentialsize
	double ** ppvi; //auxilary array for the precalculated interaction
	double ** ppvc; //auxilary array for the precalculated cloumb interaction
	double ** ppvjell; //auxilary array for the precalculated jellium potential
	long nstartMC; //number of initial MC steps (before averaging)
	long nMC; //number of MC steps used for averages
	double *pE; //succesive total energies. The energy at index 0 is the energy of the initial state
	long ncor; //number of correlation calls 
	double **pc11; //correlation matrices
	double **pc22;
	double **pc12;
	double **pc21;
	double **ppft;
	long ftdim;
	char ver[10];
	double acpt; //the acceptance ratio
	long pottype; //the potential type
	long nClHist;
	long *phist; //the array of the cluster size distribution
	long *dist; //array of size distribution for particles in diferent cluster. Joa
	double *phist2; //the array of the cluster size distribution square
	long **ppaux; //auxilary array
	long *paux1; //auxilary array 1
	long **ppauxb;
	long *conduct;
//	char **px;
//	char **py;
//	unsigned long *px;
//	unsigned long *py;
} MODEL;


/*typedef struct  tagCLUSTER{
	int size;
	int per;
	long **pcarea;
	long **pcrim;
	ELECLU *pc
} CLUSTER;*/

typedef struct tagELECLU{
	int i; //coordinates of the particle
	int j;
	int l;
	char type;// Two types of particles: 0,1
} ELECLU;

typedef struct  tagMODEL99{
	int dim; //the dimension of the system
	long npart; //the number of particles in the system
	double g2; //the coupling constant
	double anizo; //the anizotropy
	double range; //the range of interaction
	int init; //how to initialize
	PARTICLE *p; //linear array of particles of size dim*dim 
	double T; //the tmperature
	double mu; //the chemical potential
	double ** ppvi; //auxilary array for the precalculated interaction
	double ** ppvc; //auxilary array for the precalculated cloumb interaction
	double ** ppvjell; //auxilary array for the precalculated jellium potential
	long nstartMC; //number of initial MC steps (before averaging)
	long nMC; //number of MC steps used for averages
	double *pE; //succesive total energies. The energy at index 0 is the energy of the initial state
	long ncor; //number of correlation calls 
	double **pc11; //correlation matrices
	double **pc22;
	double **pc12;
	double **pc21;
	double **ppft;
	long ftdim;
	char ver[10];
	double acpt; //the acceptance ratio
	long pottype; //the potential type
	long nClHist;
	long *phist; //the array of the cluster size distribution
	long *phistl;
	long *phistr;
	long *phistu;
	long *phistd;
	long **ppaux; //auxilary array
	long **ppauxl;
	long **ppauxr;
	long **ppauxu;
	long **ppauxd;
//	char **px;
//	char **py;
//	unsigned long *px;
//	unsigned long *py;
} MODEL99;

typedef struct  tagMODEL98{
	int dim; //the dimension of the system
	long npart; //the number of particles in the system
	double g2; //the coupling constant
	double anizo; //the anizotropy
	double range; //the range of interaction
	int init; //how to initialize
	PARTICLE *p; //linear array of particles of size dim*dim 
	double T; //the tmperature
	double mu; //the chemical potential
	double ** ppvi; //auxilary array for the precalculated interaction
	double ** ppvc; //auxilary array for the precalculated cloumb interaction
	double ** ppvjell; //auxilary array for the precalculated jellium potential
	long nstartMC; //number of initial MC steps (before averaging)
	long nMC; //number of MC steps used for averages
	double *pE; //succesive total energies. The energy at index 0 is the energy of the initial state
	long ncor; //number of correlation calls 
	double **pc11; //correlation matrices
	double **pc22;
	double **pc12;
	double **pc21;
	double **ppft;
	long ftdim;
	char ver[10];
	double acpt; //the acceptance ratio
	long pottype; //the potential type
//	char **px;
//	char **py;
//	unsigned long *px;
//	unsigned long *py;
} MODEL98;


typedef struct  tagMODEL97{
	int dim; //dimension of the system
	long npart; //number of particles in the system
	double g2; //coupling constant
	double anizo; //anizotropy
	double range; //range of interaction
	int init; //how to initialize
	PARTICLE *p; //linear array of particles of size dim*dim 
	double T; //tmperature
	double mu; //chemical potential
	double ** ppvi; //auxilary array for precalculated interaction
	double ** ppvc; //auxilary array for precalculated cloumb interaction
	double ** ppvjell; //auxilary array for precalculated jellium potential
	long nstartMC; //number of initial MC steps (before averaging)
	long nMC; //number of MC steps used for averages
	double *pE; //succesive total energies. Energy at index 0 is energy of initial state
	long ncor; //number of correlation calls 
	double **pc11; //correlation matrices
	double **pc22;
	double **pc12;
	double **pc21;
	double **ppft;
	long ftdim;
	char ver[10];
//	char **px;
//	char **py;
//	unsigned long *px;
//	unsigned long *py;
} MODEL97;

	
//file.c
PARAM parread(char *fname);
void logprintf(char *, ...);
int binwrite(char *fname,MODEL *pm);
MODEL binread(char * fname);
void writeBA(MODEL *pm, LUPBA *pba,char *fname);
long readBA(MODEL *pm, LUPBA *pba,char *fname);
void writeh(MODEL *pm, LUPBA *pba,char *fname);
void write_rho_h(MODEL *pm, char *fname);

//clusterdll.c
#define DLL_EXPORT __declspec(dllexport) __stdcall
#define DLL_IMPORT __declspec(dllimport) __stdcall

void DLL_EXPORT  VBInitBA(LUPBA *pba);
void  DLL_EXPORT VBInitBA0(LUPBA *pba,MODEL *pm,double mint, double maxt);
void  DLL_EXPORT VBUpdateBA(MODEL *pm,LUPBA *poba,double mstepf);
void DLL_EXPORT VBPrepBA(LUPBA *pba);
void DLL_EXPORT VBInitRho(MODEL *pm,LUPBA *pw);
void DLL_EXPORT VBUpdateWeight(MODEL *pm,LUPBA * pw);
void DLL_EXPORT VBRangeBA(LUPBA * pw,long imin, long imax,long flag);


//process.cpp
int setidlepriority();

//ran2.c
double ran2(long *idum);

//random.c

void CLCG_srand(unsigned long x,unsigned long y);
ran2_srand(long seed);
double drand();
sel_rand(int i);

//model.c

int initialize(MODEL *pm);
void initpot(MODEL *pm);
void clean(MODEL *pm);
void seed_random(MODEL *pm);
void seed_center(MODEL *pm);
void seed_center_uniform(MODEL *pm);
double singeint(int i,MODEL *pm);
double tot_eng(MODEL *pm);
void MCStep(MODEL *pm);
void MCStepMu(MODEL *pm);
void rrcor(int mode,double part,MODEL *pm);
long FFTNextPowerOfTwo(long n);
void four_trans(MODEL *pm);
void gencor( MODEL *pm);
double getcorr(int ix, int iy,int type,MODEL *pm);
void cleancor(MODEL *pm);
double calcVL(MODEL *pm);
double calcsigrel(MODEL *pm);
void MCStep_multi(MODEL *pm,LUPBA *pba);
void cleanhist(MODEL *pm);
void count_clusters(MODEL* pm);
double lupba(double e,LUPBA *pba,MODEL *pm);
void MCStep_Neigh(MODEL *pm);
double ordpar(MODEL *pm);
void MCStepMu_multi(MODEL *pm,LUPBA *pba);
void cleanrhohist(MODEL *pm);
void MCStepMu_rho(MODEL *pm,LUPBA *pw);
void four_trans_spin(MODEL *pm);
void cor_per( MODEL *pm);
double getcorr_per(int ix, int iy,int type,MODEL *pm);
void genclustcorr( MODEL *pm);
double mobility(MODEL *pm, long k);

//unsigned getxbit(MODEL *pm,unsigned i, unsigned j);
//unsigned getybit(MODEL *pm,unsigned i, unsigned j);
//void setybit(MODEL *pm,unsigned i, unsigned j,int bit);
//void setxbit(MODEL *pm,unsigned i, unsigned j,int bit);

//util.c


char **cmatrix(nrl,nrh,ncl,nch);
void free_cmatrix(char **m, long nrl, long nrh, long ncl, long nch);
double lupfunc(double x, LUPFUNC *lp);
double luphist(double x, LUPFUNC *lp);
double luphistl(double x, LUPFUNC *lp);
LUPFUNC  histogram(double *arr,long dim,double xmin,double xmax, long dim1);
void calcCov(double *p,long n,double *avg, double *sig, double *r);
void filter(double *arr,long dim);
double lupfuncb(double e,LUPBA *pba,MODEL *pm);
double lupfuncg(double e,LUPBA *pba,MODEL *pm);
long coarseBA(double minstep,LUPBA *pba,MODEL *pm);
long setStep(double minstep,LUPBA *pba,MODEL *pm);
LUPFUNC  lhistogram(long *arr,long dim, double minx,double maxx,long hdim);
void WSmothBA(LUPBA *pba,long wnd);


//rlft3.c
void rlft3(double ***data, double **speq, unsigned long nn1, unsigned long nn2,
	unsigned long nn3, int isign);
//process.c
int setidlepriority();


#define PI 3.1415926535897932384626433832795

#define MAGIC "CLS3"
#define MAGIC99 "CLS2"
#define MAGIC98 "CLS1"
#define MAGIC97 "CLS0"
#define VERSION "1.00"
//#define SIGN(x) ((x)<0?-1:1)