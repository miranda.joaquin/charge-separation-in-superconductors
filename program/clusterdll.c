#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "nrutil.h"
#include "clustdll\stdafx.h"
#include "cluster.h"
//#include "clustdll.h"
#define SIZE 30

//MODEL mod;
//LUPBA ba;


/*long DLL_EXPORT VBGet_mp(void){
	return (long)&mod;
}*/


void DLL_EXPORT init_model(MODEL *pm){

//	pm->dim=SIZE;
//	pm->npart=SIZE*SIZE/2;
//	pm->anizo=3;
//	pm->g2=1.;
//	pm->range=5;
//	pm->T=1.;

	initialize(pm);
//	seed_random(pm);

}

void DLL_EXPORT clean_model(MODEL *pm){
	clean(pm);
}


void DLL_EXPORT init_pot(MODEL *pm){
	initpot(pm);
}


void DLL_EXPORT get_part(long i,MODEL *pm, long* ret){
	ret[0]=pm->p[i].i;
	ret[1]=pm->p[i].j;
	ret[2]=(long)pm->p[i].type;
}


void DLL_EXPORT add_part(long i,long j,MODEL *pm){

	
	pm->p[pm->npart].i=i;
	pm->p[pm->npart].j=j;
	pm->p[pm->npart++].type=(char)(drand()+.5);
}


void DLL_EXPORT VBMCStep(MODEL *pm){
//	double tmp;
	switch(pm->init){
		case 2:
			MCStepMu(pm);
			break;
		case 0:
		case 1:
		default:
			MCStep(pm);
//			tmp=pm->acpt;
//			MCStep_Neigh(pm);
//			pm->acpt+=tmp;
//			pm->acpt/=2;
			break;
	}
}



void DLL_EXPORT VBRedimE(MODEL *pm,long newdim){
	long i;
	if(NULL!=pm->pE) free_dvector(pm->pE,0,pm->nstartMC+pm->nMC);
	pm->pE=dvector(0,newdim);
	pm->nstartMC=0;
	pm->nMC=newdim;
	for(i=0;i<=pm->nstartMC+pm->nMC;i++) pm->pE[i]=0;
} 
 
void DLL_EXPORT VBRedimRh(MODEL *pm,long newdim){
	long i;
	if(NULL!=pm->phist) free_lvector(pm->phist,0,pm->nstartMC+pm->nMC);
	pm->phist=lvector(0,newdim);
	pm->nstartMC=0;
	pm->nMC=newdim;
	for(i=0;i<=pm->nstartMC+pm->nMC;i++) pm->phist[i]=0;
} 



void DLL_EXPORT VBInitBA(LUPBA *pba){
	long i;
//	double hv;
	if(NULL!=pba->pa) free_dvector(pba->pa,0,pba->dim-1);
	if(NULL!=pba->pb) free_dvector(pba->pb,0,pba->dim-1);
	if(NULL!=pba->pg) free_dvector(pba->pg,0,pba->dim-1);
	pba->pa=dvector(0,pba->dim-1);
	pba->pb=dvector(0,pba->dim-1);
	pba->pg=dvector(0,pba->dim-1);
	for(i=0;i<pba->dim;i++) {
		pba->pa[i]=0;
		pba->pb[i]=0;
		pba->pg[i]=0;
	}
}

void DLL_EXPORT VBInitBA0(LUPBA *pba,MODEL *pm, double tmin,double tmax){
	long i;
	double dx;
//	double hv;
	if(NULL!=pba->pa) free_dvector(pba->pa,0,pba->dim-1);
	if(NULL!=pba->pb) free_dvector(pba->pb,0,pba->dim-1);
	if(NULL!=pba->pg) free_dvector(pba->pg,0,pba->dim-1);
	pba->pa=dvector(0,pba->dim-1);
	pba->pb=dvector(0,pba->dim-1);
	pba->pg=dvector(0,pba->dim-1);
//	pba->minx=-1.3*pm->npart;
//	pba->maxx=0;
	tmin=1/tmin;
	tmax=1/tmax;
	dx=(tmin-tmax)/pba->dim;
	for(i=0;i<pba->dim;i++) {
//		pba->pa[i]=0;
		pba->pb[i]=tmax;//tmin-i*dx; 
		pba->pg[i]=0;
	} 
	pba->pb[0]=tmin;
	pba->pb[pba->dim-1]=tmax;


}

void DLL_EXPORT VBUpdateBA(MODEL *pm,LUPBA *poba,double mstepf){
	LUPFUNC hist;
	LUPBA nba;
	double dx,tmp,gtmp,sig;
	long i,j;
	long flag;
	long norm;
	double h0=1;
	long tresh=pm->nMC/5;

	switch(pm->init){
		default:
			norm=pm->npart;
			break;
		case 2:
		case 20:
		case 21:
		case 22:
			norm=pm->dim*pm->dim;
			break;
	}


//	poba=&ba;	
	dx=(poba->maxx-poba->minx)/(poba->dim-1);
	hist=histogram(pm->pE+pm->nstartMC+1,pm->nMC,0,0,0);
	hist.minx*=norm;
	hist.maxx*=norm;
	hist.minx-=2*dx;
	hist.maxx+=2*dx;
	if(hist.minx<=poba->minx){
	} 
	else {
		hist.minx=poba->minx;
	}
	if(hist.maxx>=poba->maxx){
	} 
	else {
		hist.maxx=poba->maxx;
	}
	calcCov(pm->pE+pm->nstartMC+1,pm->nMC,&tmp,&sig,&tmp);
	sig*=norm;
	if(sig>0 && sig<mstepf*dx) dx=sig/mstepf;
	hist.dim=(long)((hist.maxx-hist.minx)/dx)+1;
	logprintf("sig: %lg\tdx: %lg\t nhist: %ld\n",sig/norm,dx/norm,hist.dim);
	hist.minx/=norm;
	hist.maxx/=norm;
	hist=histogram(pm->pE+pm->nstartMC+1,pm->nMC,hist.minx,hist.maxx,hist.dim); 
/*	tmp=__max(1./poba->pb[0],pm->pE[0])*mstepf;
	if(dx>tmp){
		hist=histogram(pm->pE+pm->nstartMC+1,pm->nMC,hist.minx,hist.maxx,hist.dim);
		for(j=0,flag=0;dx>2*tmp && j<10 && flag==0;j++){ //Division of bins to get reasonably broad histogram.
			flag=1;
			for(i=0;i<hist.dim;i++)
				if(hist.py[i]>tresh){
					flag=0;
					free_dvector(hist.py,0,hist.dim-1);
					hist.dim=2*hist.dim;
					hist=histogram(pm->pE+pm->nstartMC+1,pm->nMC,hist.minx,hist.maxx,hist.dim);
					dx=(hist.maxx-hist.minx)*norm/(hist.dim-1);
					break;
				}
		}
	} 
	else{
		hist.dim=(long)((hist.maxx-hist.minx)*norm/tmp)+1;
		hist=histogram(pm->pE+pm->nstartMC+1,pm->nMC,hist.minx,hist.maxx,hist.dim); 
	}
//	filter(hist.py,hist.dim);
*/	nba.dim=hist.dim;
	nba.minx=hist.minx*norm;
	nba.maxx=hist.maxx*norm;
	nba.pa=0;
	nba.pb=0;
	nba.pg=0;
	VBInitBA(&nba);
	dx=(nba.maxx-nba.minx)/(nba.dim-1);
	for(i=0,j=0;i<nba.dim;i++){
		nba.pb[i]=lupfuncb(nba.minx+i*dx,poba,pm);
		nba.pg[i]=lupfuncg(nba.minx+i*dx,poba,pm)*poba->dim/nba.dim;
//		if(nba.pg[i]!=0 && j==0) j=i;
	}
/*	for(i=0;i<j;i++){
		nba.pb[i]=nba.pb[j];
	}
*/	hist.py[0]+=h0;
	for(i=1;i<nba.dim-1;i++){
		hist.py[i+1]+=h0;
		if(hist.py[i]<pm->nMC) {// && hist.py[i+1]<pm->nMC){
			tmp=hist.py[i]+hist.py[i+1];
			gtmp=tmp==0?0:hist.py[i]*hist.py[i+1]/tmp; //g0n[E]
			tmp=gtmp+nba.pg[i];
			tmp=tmp==0?0:gtmp/tmp; //g0n^[E]
			nba.pg[i]+=gtmp; //g(n+1)[E]
			nba.pb[i]+=(tmp==0?0:tmp*log(hist.py[i+1]/hist.py[i])/dx);// b(n+1)[E]
		} else {//  if(hist.py[i+1]<=pm->nMC){ // if all samples fall into a single bin we need special tratment of that b[i]
/*			tmp=hist.py[i]+1;
			gtmp=hist.py[i]*1/tmp; //g0n[E]
			tmp=gtmp+nba.pg[i];
			tmp=gtmp/tmp; //g0n^[E]
			nba.pg[i]+=gtmp; //g(n+1)[E]
			nba.pb[i]+=(tmp*log(1/hist.py[i])/dx);// b(n+1)[E]*/
			nba.pb[i]=0;
			nba.pg[i]=0;
		}/* else {
/*			tmp=hist.py[i+1]+1;
			gtmp=hist.py[i+1]*1/tmp; //g0n[E]
			tmp=gtmp+nba.pg[i];
			tmp=gtmp/tmp; //g0n^[E]
			nba.pg[i]+=gtmp; //g(n+1)[E]
			nba.pb[i]+=(tmp*log(hist.py[i+1]/1)/dx);// b(n+1)[E]
			nba.pb[i+1]=0;
			nba.pg[i+1]=0;
		}
*/
		if(nba.pb[i]<0){ 
			nba.pb[i]=0;
			nba.pg[i]=0;
		}
//		nba.pg[i]*=.9; // finite memory time
	}
	nba.pb[i]=poba->pb[poba->dim-1];//nba.pb[i-1];
	nba.pg[i]=0;
	nba.pb[0]=poba->pb[0];//nba.pb[i-1];
	nba.pg[i]=0;
/*	for(i=1;i<nba.dim-1;i++){
		if(nba.pb[i]>nba.pb[0]) nba.pb[i]=nba.pb[0];
		if(nba.pb[i]<nba.pb[nba.dim-1]) nba.pb[i]=nba.pb[nba.dim-1];
	}
*/	tmp=nba.pb[nba.dim-1];
	for(i=nba.dim-2;i>0;i--){
		if(nba.pb[i]>nba.pb[0]) nba.pb[i]=nba.pb[0];
		if(nba.pb[i]<nba.pb[nba.dim-1]) nba.pb[i]=nba.pb[nba.dim-1];
		if(nba.pb[i]<tmp){
			gtmp=nba.pg[i]+1;
			nba.pb[i]=tmp/gtmp+nba.pg[i]*nba.pb[i]/gtmp;
		} else
			tmp=nba.pb[i];

	}

	free_dvector(hist.py,0,hist.dim-1);
	free_dvector(poba->pa,0,poba->dim-1);
	free_dvector(poba->pb,0,poba->dim-1);
	free_dvector(poba->pg,0,poba->dim-1);
	(*poba)=nba;
	poba->pa[poba->dim-1]=0;
	for(i=poba->dim-1;i>0;i--){
		poba->pa[i-1]=poba->pa[i]+(poba->pb[i-1]-poba->pb[i])*(dx*i+poba->minx);
	}

}




void DLL_EXPORT VBMCStep_multi(MODEL *pm,LUPBA * pba){
//	pba=&ba;
	switch(pm->init){
		case 2:
			MCStepMu_multi(pm,pba);
			break;
		case 0:
		case 1:
		default:
			MCStep_multi(pm,pba);
//			tmp=pm->acpt;
//			MCStep_Neigh(pm);
//			pm->acpt+=tmp;
//			pm->acpt/=2;
			break;
	}


}



double DLL_EXPORT VBGetB(long i,LUPBA *pba){
//	pba=&ba;
	return pba->pb[i];
}


void DLL_EXPORT VBSetB(long i,double b,LUPBA *pba){
//	pba=&ba;
	pba->pb[i]=b;
}


void DLL_EXPORT VBPrepBA(LUPBA *pba){
	long i;
	double dx;

	dx=(pba->maxx-pba->minx)/(pba->dim-1);
	pba->pa[pba->dim-1]=0;
	for(i=pba->dim-1;i>0;i--){
		pba->pa[i-1]=pba->pa[i]+(pba->pb[i-1]-pba->pb[i])*(dx*i+pba->minx);
	}
}

double DLL_EXPORT VBGetW(long i,LUPBA *pba,MODEL *pm){
	double dx;
	dx=(pba->maxx-pba->minx)/(pba->dim-1);
	return lupba(i*dx+pba->minx,pba,pm);
}



double  DLL_EXPORT VBtot_eng(MODEL *pm){
//	long i;
	switch(pm->init){
		case 2:
//			pm->npart=pm->dim*pm->dim;
//			for(i=0;i<pm->npart;i++) pm->p[i].type=0;
			return tot_eng(pm);//-pm->mu;
			break;
		case 0:
		case 1:
		default:
			return tot_eng(pm);
	}
}


double  DLL_EXPORT VBTDpot(MODEL *pm){
	if(pm->init==2)
			return tot_eng(pm)-pm->mu*pm->npart;

	return tot_eng(pm);

}


//void DLL_EXPORT VBrrcor(long mode,double part, MODEL *pm){
//
//	rrcor(mode,part,pm);
//}

void DLL_EXPORT VBcor_per(MODEL *pm){

	cor_per(pm);
}

 
void DLL_EXPORT VBgencor(MODEL *pm){

	gencor(pm);
}

void DLL_EXPORT VBcleancor(MODEL *pm){

	cleancor(pm);
}


double DLL_EXPORT VBgetcorr(long i, long j,long type,MODEL *pm){
//	if(i==0 && j==0) return 0;
	return getcorr(i,j,type,pm);
}


void DLL_EXPORT VBfft(MODEL *pm){
//	four_trans(pm);
    four_trans_spin(pm);
}

double DLL_EXPORT getft(long i, long j,MODEL *pm){
	return pm->ppft[i][j];
}

void DLL_EXPORT VBbinread(char *fname,MODEL *pm){
	*pm=binread(fname);
}

double DLL_EXPORT VBGetE(long i,MODEL *pm){
	return pm->pE[i];
}

double DLL_EXPORT VBGetVjell(long i, long j,MODEL *pm){
	return pm->ppvjell[i][j];
}

void DLL_EXPORT VBSetE(long i, double e,MODEL *pm){
	pm->pE[i]=e;
}

void DLL_EXPORT VBCountClusters(MODEL* pm){

	count_clusters(pm);
}

double DLL_EXPORT VBGetHist(long i,MODEL *pm){
	if(0==pm->nClHist) count_clusters(pm);
	return pm->phist[i]/(double)pm->nClHist;
}

void DLL_EXPORT VBCleanClustHist(MODEL* pm){

	cleanhist(pm);
}


void DLL_EXPORT VBCalcEHist(MODEL *pm,LUPFUNC *hst){

	*hst=histogram(pm->pE+pm->nstartMC+1,pm->nMC,hst->minx,hst->maxx,hst->dim);
//	filter(hst->py,hst->dim);
}

void DLL_EXPORT VBCleanEHist(LUPFUNC *hst){

	if(NULL!=hst->py){
		free_dvector(hst->py,0,hst->dim-1);
		hst->py=0;
	}
}

double DLL_EXPORT VBGetEHist(long i,LUPFUNC *hst){

	return hst->py[i];
}


long DLL_EXPORT VBreadBA(MODEL *pm, LUPBA *pba,char *fname){
	return(readBA(pm,pba,fname));
}

double DLL_EXPORT VBGetDbl(long i,double * ptr){

	return ptr[i];
}

void DLL_EXPORT VBSetDbl(long i,double * ptr,double val){

	ptr[i]=val;
}

void DLL_EXPORT VBSetLng(long i,long * ptr,long val){

	ptr[i]=val;
}


long DLL_EXPORT VBAllocDbl(long i){
	return (long) lvector(0,i);
}

void DLL_EXPORT VBFreeDbl(long * ptr){
	free_lvector(ptr,0,0);
}

void DLL_EXPORT VBUpdateWeight(MODEL *pm,LUPBA * pw){
	long i;
	long norm;
	double *ph;
	double tmp,gtmp;
	double wg;

	norm=pm->dim*pm->dim;
	ph=dvector(0,norm);
	for(i=0;i<=norm;i++) ph[i]=1;
	for(i=pm->nstartMC+1;i<pm->nstartMC+1+pm->nMC;i++){
		ph[pm->phist[i]]+=1;
	}
	wg=10;
	for(i=0;i<norm;i++){
		tmp=ph[i]+ph[i+1];
		gtmp=tmp==0?0:ph[i]*ph[i+1]/tmp; //g0n[E]
		tmp=wg*gtmp+pw->pg[i];
		tmp=tmp==0?0:wg*gtmp/tmp; //g0n^[E]
		pw->pg[i]+=gtmp; //g(n+1)[E]
		pw->pa[i]=(tmp==0?0:tmp*log(ph[i+1]/ph[i]));
		pw->pb[i]+=pw->pa[i];// b(n+1)[E]
	}

//	WSmothBA(pw,pw->dim/100);
/*	for(i=0;i<=norm;i++){
		tmp=1-1/ph[i];
		pw->pg[i]+=tmp;
//		pw->pb[i]=pw->pg[i]*pw->pb[i]+tmp*log(ph[i]);
		pw->pb[i]+=log(ph[i]);
//		pw->pb[i]/=pw->pg[i];
//		pw->pb[i]=exp(-pw->pb[i]);
	}
*///	tmp=pw->pb[0];
//	for(i=1;i<=norm;i++) if(pw->pb[i]>tmp) tmp=pw->pb[i];
//	for(i=0;i<=norm;i++) pw->pb[i]/=tmp;
	pw->pa[pw->dim-1]=0;
	for(i=pw->dim-1;i>0;i--){
		pw->pa[i-1]=pw->pa[i]+(pw->pb[i-1]-pw->pb[i])*i;
	}

	free_dvector(ph,0,norm);

}


void DLL_EXPORT VBMCStepRho(MODEL *pm,LUPBA *pw){
	MCStepMu_rho(pm,pw);
}

void DLL_EXPORT VBInitRho(MODEL *pm,LUPBA *pw){
	long i;
	long norm;

	norm=pm->dim*pm->dim;
	pw->dim=norm+1;
	pw->minx=0;
	pw->maxx=norm;
	pw->pb=dvector(0,norm);
	pw->pg=dvector(0,norm);
	pw->pa=dvector(0,norm);
	for(i=0;i<=norm;i++){
		pw->pb[i]=0;
		pw->pg[i]=0;
	}

}

void DLL_EXPORT VBCalcRhoHist(MODEL *pm,LUPFUNC *hst){
	double *p,tmp;
	long i;

	tmp=pm->dim*pm->dim;
	p=dvector(0,pm->nMC);
	for(i=0;i<pm->nMC;i++){
		p[i]=pm->phist[pm->nstartMC+1+i]/tmp;
	}
	*hst=histogram(p,pm->nMC,hst->minx,hst->maxx,hst->dim);
//	filter(hst->py,hst->dim);
}

void DLL_EXPORT VBRangeBA(LUPBA * pw,long imin, long imax,long flag){
	long i;

	for(i=0;i<imin;i++)
		pw->pb[i]=-1e6;
	if(flag==0)
		for(i=imin;i<imax;i++)
			pw->pb[i]=0;

	for(i=imax;i<pw->dim;i++)
		pw->pb[i]=1e6;
}

int DLL_EXPORT VBLoadBA(LUPBA * pba,MODEL *pm,char *fname){
	return readBA(pm,pba,fname);
}

int DLL_EXPORT VBSaveBA(LUPBA * pba,MODEL *pm,char *fname){
	writeBA(pm,pba,fname);
	return 0;
}