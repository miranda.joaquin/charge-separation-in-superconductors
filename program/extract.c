#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "cluster.h"
#include "nrutil.h"

//long bin[1000];

extern FILE* fplog;

main(int argc, char **argv){

	MODEL mod;
	long i,ix,iy;
	long ctype,range;
	double cor;
	double dir=0;
	char rtype='r';
	int count;
	double xpar,rhoavg;;


	//setidlepriority();
	if((argc>1) && ('-'==argv[1][0])){
		printf("xpar\t");
		printf("corr\t");
		printf("dim\t");
		printf("fill\t");
		printf("coup\t");
		printf("anizo\t");
		printf("range\t");
		printf("temp\t");
		printf("VL\t");
		printf("sigrel\t");
		printf("\n");
		exit(0);
	}

	for ( count = 0; count <= argc   ; count++)//Joa
	{
				/* printf expects a pointer 
				 * to the text 
				 */

		printf("parm %d is %s\n", count, argv[count]);
	
	}//Joa

	if(argc<3){
		logprintf("Usage: extract -|(binfile.bin corrtype (/i:ix-iy) | (/r:range [direction]))");
				// | Bitewise Operator, compares each bit of its first operand to the corresponding bit of its second operand.  
			    //	If either bit is 1,the corresponding result bit is set to 1. Otherwise, the corresponding result bit is set to 0.Joa
		exit (1);

	}
	//printf("I'm here");//Joa

	mod=binread(argv[1]);
	if(0==mod.dim) exit(1);
	if(0==mod.ncor) genclustcorr(&mod); //gencor(&mod);
	range=mod.dim;
	rhoavg=mod.npart/((double)mod.dim*mod.dim);
	if(0==strcmp(argv[2],"rr"))
		ctype=0;
	else{
		ctype=atoi(argv[2]);
		if(0==ctype){ 
			fprintf(stderr,"Wrong correlation type specification: %s\n",argv[2]);
			exit(1);
		}
	}
	if(argc>3)
		//printf("I'm here");Joa
		if('/'==argv[3][0]){
			rtype=argv[3][1];
			switch(rtype){
				case 'r':
					if(1!=sscanf(argv[3],"/r:%ld",&range)){
						fprintf(stderr,"Wrong range specification: %s\n",argv[3]);
						exit(1);
					}
					range=range<mod.dim?range:mod.dim;
					if(argc>4){
						dir=atan(1.)/45.*atof(argv[4]);
					}
					break;
				case 'i':
					if(2!=sscanf(argv[3],"/i:%ld-%ld",&ix,&iy)){
						fprintf(stderr,"Wrong range specification: %s\n",argv[3]);
						exit(1);
					}
					if(mod.dim<=labs(ix) || mod.dim<=labs(iy)){
						fprintf(stderr,"Indexes out of range: %s\n",argv[3]);
						exit(1);
					}
					break;
				default:
					fprintf(stderr,"Wrong range specification: %s\n",argv[3]);
					break;
			}
		}
		else
			fprintf(stderr,"Wrong range specification: %s\n",argv[3]);
//	for(i=mod.nstartMC;i<=mod.nMC+mod.nstartMC;i++)
//		printf("%ld %lg\n",i,mod.pE[i]);
	fprintf(stderr,"%s\n",argv[1]);
//	printf("%ld\t",i);
//	i=strcspn(strchr(argv[1],'-'),"0123456789");
//	printf("%ld\n",i); 
//	printf(strchr(argv[1],'-'));
	switch(rtype){
		case 'r':
			printf("dist\t");
			switch(ctype){ 
				case 0:
				default:
					printf("rrr\n");
					break;
				case 11:
					printf("r11\n");
					break;
				case 22:
					printf("r22n");
					break;
				case 12:
					printf("r12\n");
					break;
				case 21:
					break;
					printf("r21\n");
					break;
			}

			for(i=-range;i<=range;i++){
				cor=0;
				iy=(long)(i*sin(dir));
				ix=(long)(i*cos(dir));
				cor=getcorr(ix,iy,ctype,&mod);
				printf("%ld\t%lg\n",i,cor);
			}
//			printf("\n");
			break;
		case 'i':
				sscanf(strrchr(argv[1],'-')+1,"%lg",&xpar);
				printf("%lg\t",xpar);
				cor=getcorr(ix,iy,ctype,&mod);
				cor+=getcorr(-iy,ix,ctype,&mod);
				cor/=2;
				printf("%lg\t",cor);
				printf("%ld\t",mod.dim);
				printf("%lg\t",mod.npart/(double)mod.dim/(double)mod.dim);
				printf("%lg\t",mod.g2);
				printf("%lg\t",mod.anizo);
				printf("%lg\t",mod.range);
				printf("%lg\t",mod.T);
				printf("%lg\t",calcVL(&mod));
				printf("%lg\t",calcsigrel(&mod));
				switch(ctype){ 
					case 0:
					default:
						printf("rrr\t");
						break;
					case 11:
						printf("r11\t");
						break;
					case 22:
						printf("r22\t");
						break;
					case 12:
						printf("r12\t");
						break;
					case 21:
						break;
						printf("r21\t");
						break;
					}
				printf("%s\n",argv[1]);

//				printf("\n");
			break;
	}
	clean(&mod);
}