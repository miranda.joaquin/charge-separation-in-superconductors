#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <string.h>
#include "nrutil.h"
#include "cluster.h"
//#include <cmath>



//unsigned long set_lkt[sizeof(unsigned long)];
//unsigned long reset_lkt[sizeof(unsigned long)];
//int ull=sizeof(unsigned long);

_inline double vcol(int i, int j){
	return 1./sqrt(i*i+j*j);
}

_inline double vjell(int i, int j,int N){

	return -(((-((0.5 + j)*log(-0.5 - i + sqrt(pow(0.5 + i,2) + pow(0.5 + j,2)))) - 
         (0.5 + i)*log(-0.5 - j + sqrt(pow(0.5 + i,2) + pow(0.5 + j,2))) + 
         (0.5 + i)*log(-0.5 - j + sqrt(pow(0.5 + j,2) + pow(0.5 + i - N,2))) - 
         N*log(-0.5 - j + sqrt(pow(0.5 + j,2) + pow(0.5 + i - N,2))) + 
         (0.5 + j)*log(-0.5 - i + sqrt(pow(0.5 + i,2) + pow(0.5 + j - N,2))) - 
         N*log(-0.5 - i + sqrt(pow(0.5 + i,2) + pow(0.5 + j - N,2))) + 
         (0.5 + j)*log(-0.5 - i + sqrt(pow(0.5 + j,2) + pow(0.5 + i - N,2)) + N) + 
         (0.5 + i)*log(-0.5 - j + sqrt(pow(0.5 + i,2) + pow(0.5 + j - N,2)) + N) - 
         (0.5 + j)*log(-0.5 - i + sqrt(pow(0.5 + i - N,2) + pow(0.5 + j - N,2)) + N) + 
         N*log(-0.5 - i + sqrt(pow(0.5 + i - N,2) + pow(0.5 + j - N,2)) + N) - 
         (0.5 + i)*log(-0.5 - j + sqrt(pow(0.5 + i - N,2) + pow(0.5 + j - N,2)) + N) + 
         N*log(-0.5 - j + sqrt(pow(0.5 + i - N,2) + pow(0.5 + j - N,2)) + N)))/pow(N,2));
}

_inline double vint(int i, int j,MODEL *pm){
	double r2;
	double i2,j2;
	double norm;

	r2=i*i+j*j;
	i2=i*i;
	j2=j*j;
	norm=pm->range*pm->range*pm->range*pm->range*exp(-4.);		//norm=range*range*exp(-4).Joa
//	return -pm->g2*(r2-pm->anizo*labs(i*j))*exp(-sqrt(r2)/pm->range);
//	return -pm->g2*((i2-j2)*(i2-j2)/4-pm->anizo*i2*j2)*exp(-sqrt(r2)/pm->range)/r2/r2;
	return -pm->g2*((i2-j2)*(i2-j2)/4-pm->anizo*i2*j2)*exp(-4*sqrt(r2)/pm->range)/norm;//g2*(i2-j2)*(i2-j2)- anizo*i2*j2*exp(-4sqrt(r2)/range)/norm

}

_inline double vint1(int i, int j,MODEL *pm){
	double r2;

	r2=i*i+j*j;
	if(1==r2) return pm->g2*vcol(1,0);
	if(2==r2) return pm->range*vcol(1,0);
	if(4==r2) return pm->anizo*vcol(1,0);
	return 0;

}

_inline double vint_per(int i, int j,MODEL *pm){
	double r2;

	i=labs(i);
	j=labs(j);
	i=__min(i,pm->dim-i);
	j=__min(j,pm->dim-j);
	r2=i*i+j*j;
	if(1==r2) return pm->g2*vcol(1,0);
	if(2==r2) return pm->range*vcol(1,0);
	if(4==r2) return pm->anizo*vcol(1,0);
	return 0;

}


_inline double f(int i, int j){
	return 1./sqrt(i*i+j*j);
}


_inline double S(int m,int n,int dim,long nmax){
	int i,j;
	double sum=0;

	nmax*=dim;
	for(i=dim;i<nmax;i+=dim){
		for(j=dim;j<nmax;j+=dim){
			sum+=f(m+i,n+j) + f(m-i,n-j)+f(m+i,n-j)+f(m-i,n+j)-4*f(i,j);
		}
	}
	for(i=dim;i<nmax;i+=dim){
		sum+=f(m+i,n)+f(m-i,n)+f(m,n+i)+f(m,n-i)-4*f(i,0);
	}
	sum+=f(m,n);
	return sum;
}

_inline double ift(long m, long n, long dim, double ** ppvck){
	long i,j;
	double ret,ph;

	ret=0;
	ph=2.*PI/dim;
	for(i=0;i<dim;i++)
		for(j=0;j<dim;j++){
			ret+=cos(ph*(i*m+j*n))*ppvck[i][j];
		}
	ret/=dim;
	return ret;
}

_inline void calc_vcol_per(MODEL *pm){//Rutine for assing vck =S and ppvc=ift. Comes from initpotencial
	double **vck;
	long i,j;

	vck=dmatrix(0,pm->dim,0,pm->dim);	
	for(i=0;i<pm->dim;i++){
		for(j=i+1;j<pm->dim;j++){
			vck[i][j]=S(i,j,pm->dim,100);//S is contribution of the form 1/(i*i+j*j). Why up to 100?. Joa
			vck[j][i]=vck[i][j];
		}
	}
	for(i=1;i<pm->dim;i++){
		vck[i][i]=S(i,i,pm->dim,100);
	}
	vck[0][0]=0;
	for(i=0;i<pm->dim;i++){// Non diagonal terms?. Joa
		for(j=i+1;j<pm->dim;j++){				//ppvc auxilary array for the precalculated cloumb interaction
			pm->ppvc[i][j]=ift(i,j,pm->dim,vck);//ift of S contribution trought vck, S is a sum of terms like 1/(i*i+j*j). Joa
			pm->ppvc[-i][-j]=pm->ppvc[i][j];
			pm->ppvc[i][-j]=pm->ppvc[i][j];
			pm->ppvc[-i][j]=pm->ppvc[i][j];
			pm->ppvc[j][i]=pm->ppvc[i][j];
			pm->ppvc[-j][-i]=pm->ppvc[i][j];
			pm->ppvc[j][-i]=pm->ppvc[i][j];
			pm->ppvc[-j][i]=pm->ppvc[i][j];
		}
	}
	for(i=1;i<pm->dim;i++){//Diagonal terms
			pm->ppvc[i][i]=ift(i,i,pm->dim,vck);
			pm->ppvc[-i][-i]=pm->ppvc[i][i];
			pm->ppvc[i][-i]=pm->ppvc[i][i];
			pm->ppvc[-i][i]=pm->ppvc[i][i];
	}
	pm->ppvc[0][0]=0;
	free_dmatrix(vck,0,pm->dim,0,pm->dim);
}



void initpot(MODEL *pm){//Goes to rutine for getting parameters g2, anizo, range.Joa
	int kx,ky;
	double Ejell;
	double (*pvint)(int,int,MODEL *);

	switch(pm->pottype){
		case 0:
		default:
			pvint=vint;
			break;
		case 1:
		case 2:
			pvint=vint1;
			break;
		case 3:
		case 4:
			pvint=vint_per;
			break;
	}

	switch(pm->pottype){
		case 0:
		case 1:
		default:	//Coulomb+pvint+vjell+Ejell. Joa
			for(kx=-pm->dim;kx<=pm->dim;kx++)
				for(ky=-pm->dim;ky<=pm->dim;ky++){
					if(labs(kx)+labs(ky)!=0){
						pm->ppvc[kx][ky]=vcol(kx,ky);	/// takes coulomb interaction. Joa
						pm->ppvi[kx][ky]=pvint(kx,ky,pm);///takes parameter values. g2,anizo, range. Joa
					}
				}
			Ejell=(2.*(1-sqrt(2))+3.*log((sqrt(2.)+1.)/(sqrt(2.)-1.)))/(3.*pm->dim);//Potential energy of jellium
			for(kx=0;kx<pm->dim;kx++)
				for(ky=0;ky<pm->dim;ky++){
						pm->ppvjell[kx][ky]=vjell(kx,ky,pm->dim)+Ejell;
				}
			break;
		case 2:
		case 3:	// only pvint. Joa
			for(kx=-pm->dim;kx<=pm->dim;kx++)
				for(ky=-pm->dim;ky<=pm->dim;ky++){
					if(labs(kx)+labs(ky)!=0){
						pm->ppvc[kx][ky]=0;//vcol(kx,ky);
						pm->ppvi[kx][ky]=pvint(kx,ky,pm);
					}
				}
//			Ejell=1./3./pm->dim*(2.*(1-sqrt(2))+3.*log((sqrt(2.)+1.)/(sqrt(2.)-1.)));//Potential energy of jellium
			for(kx=0;kx<pm->dim;kx++)
				for(ky=0;ky<pm->dim;ky++){
						pm->ppvjell[kx][ky]=0;//vjell(kx,ky,pm->dim)+Ejell;
				}
			break;
		case 4: //Coulomb + interaction; periodic boundary conditions
			for(kx=-pm->dim;kx<=pm->dim;kx++)
				for(ky=-pm->dim;ky<=pm->dim;ky++){
					if(labs(kx)+labs(ky)!=0){
						pm->ppvi[kx][ky]=pvint(kx,ky,pm);
					}
				}
			for(kx=0;kx<pm->dim;kx++)
				for(ky=0;ky<pm->dim;ky++){
						pm->ppvjell[kx][ky]=0;//vjell(kx,ky,pm->dim)+Ejell;
				}
			calc_vcol_per(pm);
/*			for(kx=-pm->dim+1;kx<pm->dim;kx++){
				for(ky=-pm->dim+1;ky<pm->dim;ky++){
					logprintf("%lg\t",pm->ppvc[kx][ky]);
				}
				logprintf("\n");
			}
			exit(0);
*/			break;
	}

}

int initialize(MODEL *pm){	//It's called by initialize(&mod). mod and pm are same kind of pointers class. Joa
//	int i;
//	unsigned long ul;

	pm->p=(PARTICLE*)malloc(pm->dim*pm->dim*sizeof(PARTICLE));
	pm->bip=(BIPARTICLE*)malloc(pm->dim*(pm->dim)*(pm->dim)*sizeof(BIPARTICLE));
	pm->npart=pm->npart<=pm->dim*pm->dim?pm->npart:pm->dim*pm->dim;
	pm->pE=dvector(0,pm->nstartMC+pm->nMC);						//assigned space to energy's array
	pm->ppvc=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->ppvi=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->ppvjell=dmatrix(0,pm->dim-1,0,pm->dim-1);
	//pm->pc=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);//Joa
	pm->pc11=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->pc22=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->pc12=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->pc21=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->ftdim=FFTNextPowerOfTwo(pm->dim)/2;
	pm->ppft=dmatrix(-pm->ftdim,pm->ftdim,0,pm->ftdim);
//	pm->ppaux=lmatrix(0,pm->dim-1,0,pm->dim-1);	//long **lmatrix(long nrl, long nrh, long ncl, long nch).Space assignation to ppaux.Joa
	pm->ppaux=lmatrix(-pm->dim,pm->dim+pm->dim-1,-pm->dim,pm->dim+pm->dim-1);
	pm->conduct=dvector(0,100*pm->nMC);	//pm->conduct=dvector(0,10*pm->nMC+1);
	switch(pm->init){	//What is it doing?. init is option for initialize cluster counting?
		default:	
			pm->phist=lvector(1,pm->npart);	//phist=lvector=paux1?? Joa
			//pm->phistl=lvector(1,pm->npart);//
			pm->phist2=dvector(1,pm->npart);
			//pm->phistl2=dvector(1,pm->npart);//
			pm->paux1=lvector(1,pm->npart);
			//pm->pauxl=lvector(1,pm->npart);//
			cleanhist(pm);
			break;
		case 2:
		case 20:
		case 21:
		case 22:
//			pm->phist=lvector(1,pm->dim*pm->dim);
			pm->phist=lvector(0,pm->nstartMC+pm->nMC);
			//pm->phistl=lvector(0,pm->nstartMC+pm->nMC);
			pm->phist2=0;
			//pm->phistl2=0;
//			pm->phist2=dvector(1,pm->dim*pm->dim);
			pm->paux1=0;//Initialize counter for counting hole clusters?Joa
			//pm->pauxl=0;
			break;
	}
	//pm->ppauxl=lmatrix(0,pm->dim-1,0,pm->dim-1);
	initpot(pm);
	switch(pm->init){
		case 0:
		default:
			seed_random(pm);
			break;
		case 1:
			seed_center(pm);
			break;
		case 21:
			seed_center_uniform(pm);
			break;

	}


//	for(i=0,ul=1;i<ull;i++,ul<<1){
//		set_lkt[i]=ul;
//		reset_lkt[i]=~ul;
//	}
//	pm->px1=cmatrix(0,pm->dim-1,0,pm->dim-1);
//	pm->py1=cmatrix(0,pm->dim-1,0,pm->dim-1);
//	pm->px=lvector(0,pm->dim*pm->dim/sizeof(unsigned long)+1);
//	pm->py=lvector(0,pm->dim*pm->dim/sizeof(unsigned long)+1);
	return 0;
}

void clean(MODEL *pm){
//	free_cmatrix(pm->px,0,pm->dim-1,0,pm->dim-1);
//	free_cmatrix(pm->py,0,pm->dim-1,0,pm->dim-1);
	free(pm->p);
//	free(pm->bip);

	free_dmatrix(pm->ppvi,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->ppvc,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->ppvjell,0,pm->dim-1,0,pm->dim-1);
	//free_dmatrix(pm->pc,-pm->dim,pm->dim,-pm->dim,pm->dim);//Joa
	free_dmatrix(pm->pc11,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->pc22,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->pc12,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->pc21,-pm->dim,pm->dim,-pm->dim,pm->dim);
//	free_lmatrix(pm->ppaux,0,pm->dim-1,0,pm->dim-1);
	free_lmatrix(pm->ppaux,-pm->dim,pm->dim+pm->dim-1,-pm->dim,pm->dim+pm->dim-1);
//	free_dvector(pm->conduct,0,pm->nMC+1);
	switch(pm->init){
		default:	
			free_lvector(pm->phist,1,pm->npart);
			//free_lvector(pm->phistl,1,pm->npart);
			free_dvector(pm->phist2,1,pm->npart);
			//free_dvector(pm->phistl2,1,pm->npart);
			free_lvector(pm->paux1,1,pm->npart);
			//free_lvector(pm->pauxl,1,pm->npart);
			break;
		case 2:
		case 20:
		case 21:
		case 22:
//			free_lvector(pm->phist,1,pm->dim*pm->dim);
			free_lvector(pm->phist,0,pm->nstartMC+pm->nMC);
			//free_lvector(pm->phistl,0,pm->nstartMC+pm->nMC);
//			free_dvector(pm->phist2,1,pm->dim*pm->dim);
//			free_lvector(pm->paux1,1,pm->dim*pm->dim);
			break;
	}
	//free_lmatrix(pm->ppauxl,0,pm->dim-1,0,pm->dim-1);
	if(NULL!=pm->ppft) free_dmatrix(pm->ppft,-pm->ftdim,pm->ftdim,0,pm->ftdim);
	if(NULL!=pm->pE) free_dvector(pm->pE,0,pm->nstartMC+pm->nMC);
	if(NULL!=pm->conduct) free_dvector(pm->conduct,0,100*pm->nMC);
//	if(NULL!=pm->bip)free_dvector(pm->bip,0,2*pm->dim*(pm->dim-1)*sizeof(BIPARTICLE));
	pm->p=0;
	pm->ppvi=0;
	pm->ppvc=0;
	pm->pE=0;
	pm->bip=0;
	//pm->pc=0;//Joa
	pm->pc11=0;
	pm->pc22=0;
	pm->pc12=0;
	pm->pc21=0;
	pm->ppft=0;
}

_inline swappart(int i, int j,MODEL *pm){
	PARTICLE tmp;

	tmp=pm->p[i];
	pm->p[i]=pm->p[j];
	pm->p[j]=tmp;

}



void seed_random(MODEL *pm){	//Call from main.c (seed_random(&mod))Only runs at the initial Temperature 
	long i,j,dim;

	dim=0;
	for(i=0;i<pm->dim;i++) //initalization of the Grids with coordatenes x and y (Joquin)
		for(j=0;j<pm->dim;j++){
			pm->p[dim].i=i;
			pm->p[dim].j=j;
			dim++;
		}

	for(i=0,dim=pm->dim*pm->dim;i<pm->npart;i++,dim--){

		j=i+(long)(dim*drand()); //Random generator of a integer number for j
		swappart(i,j,pm);		// exchange of  particle i for j, the new random assignation to i 
		pm->p[i].type=(char)(drand()+.5); //create random spin for the new asigation of particle i
	}
}


void seed_center(MODEL *pm){
	long i,j,dim,n,k;

	dim=0;
	for(i=0;i<pm->dim;i++)
		for(j=0;j<pm->dim;j++){
			pm->p[dim].i=i;
			pm->p[dim].j=j;
			dim++;
		}

	dim=(long)sqrt((double)pm->npart)/2+1;
//	fprintf(stderr,"%ld\n",dim);
	k=0;
	for(i=pm->dim/2-dim;i<pm->dim/2+dim && k<pm->npart;i++)
		for(j=pm->dim/2-dim;j<pm->dim/2+dim && k<pm->npart;j++){
			n=i*pm->dim+j;
			swappart(k,n,pm);
			pm->p[k].type=(char)(drand()+.5);
			k++;
		}
}

void seed_center_uniform(MODEL *pm){
	long i,j,dim,n,k;

	dim=0;
	for(i=0;i<pm->dim;i++)
		for(j=0;j<pm->dim;j++){
			pm->p[dim].i=i;
			pm->p[dim].j=j;
			dim++;
		}

	dim=(long)sqrt((double)pm->npart)/2+1;
//	fprintf(stderr,"%ld\n",dim);
	k=0;
	for(i=pm->dim/2-dim;i<pm->dim/2+dim && k<pm->npart;i++)
		for(j=pm->dim/2-dim;j<pm->dim/2+dim && k<pm->npart;j++){
			n=i*pm->dim+j;
			swappart(k,n,pm);
			pm->p[k].type=0;
			k++;
		}
}


_inline double pot(int i,int j,MODEL *pm){// Sum of coulomb potential + vint(g2 +anizo +range)
	double ret;
	long kx,ky;
//	double zero=0;

	kx=pm->p[i].i-pm->p[j].i;
	ky=pm->p[i].j-pm->p[j].j;
//	ret=vcol(kx,ky);
	ret=pm->ppvc[kx][ky];// ppvc = vcol(kx,ky).Joa
//	ret+=pm->g2*(pm->p[i].type==pm->p[j].type?1.:-1.)*vint(kx,ky,pm);
	ret+=(pm->p[i].type==pm->p[j].type?1.:-1.)*pm->ppvi[kx][ky];	//ppvi is in intpot. Takes parameter values. g2,anizo, range. Joa
//	if(fabs(ret)>100){
//        zero=0;
//    }

	return ret;
}

_inline double singeint(int i,MODEL *pm){
	long dim,n;
	double St=0.15;
	double ret;
	
	n=i<pm->npart?i:pm->npart;
	ret=0;
	ret+=(pm->p[i].type==0?1.:-1.)*St;//Including uniaxial stress
	for(dim=0;dim<n;dim++){
		ret+=pot(i,dim,pm);
	}

	for(dim=n+1;dim<pm->npart;dim++){
		ret+=pot(i,dim,pm);
	}
	ret+=pm->ppvjell[pm->p[i].i][pm->p[i].j]*pm->npart;
	return ret;
}



double tot_eng(MODEL *pm){	//From mod.pE[iMC]=tot_eng(&mod)/mod.npart, pE[iMC] energy of two next steps. Joa
	long dim,n;
	double ret;
	double St=0.15;
	ret=0;	//Sum of ppvjell + pot [Sum of coulomb potential + vint(= g2 +anizo +range)]. Joa
	for(dim=0;dim<pm->npart;dim++){
		ret+=pm->ppvjell[pm->p[dim].i][pm->p[dim].j]*pm->npart;
		ret+=(pm->p[dim].type==0?1.:-1.)*St; //Including uniaxial stress
		for(n=dim+1;n<pm->npart;n++){
			ret+=pot(dim,n,pm);
		}
	}
	return ret;
}


double ordpar(MODEL *pm){
	long i;
	long j;
	for(i=0,j=0;i<pm->npart;i++){
		j+=pm->p[i].type;
	}
	return (j-0.5*pm->npart)*2/(pm->dim*pm->dim);
}


void MCStep(MODEL *pm){ //one MC step
	long i;
	long dim,n;
	double e;
	double dr;
//	double etmp;
//	double foo;

	n=pm->dim*pm->dim-pm->npart;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){

 		dim=pm->npart+(long)(n*drand());
		pm->p[dim].type=(char)(drand()+.5);
//		e=tot_eng(pm);
		e=singeint(i,pm);
		swappart(i,dim,pm);
//		e-=tot_eng(pm);
		e-=singeint(i,pm);

//		if(fabs(etmp/e-1)>1e-6)
//			fprintf(stderr,"%lg ",etmp/e-1);
		if(!(e>=0 || drand()<=exp(e/pm->T))){
			swappart(i,dim,pm);
			pm->acpt-=1;
		}
	}
	pm->acpt/=pm->npart;
}




_inline long find_neighb_holes(long i,long *nlist,MODEL *pm){
	long n,in;
	n=0;

	if(pm->p[i].i>0){
		in=pm->p[i].i-1;
		if(pm->p[i].j>0) 
			if(pm->ppaux[in][pm->p[i].j-1]>=pm->npart){
				nlist[n]=pm->ppaux[in][pm->p[i].j-1];
				n++;
			}
		if(pm->ppaux[in][pm->p[i].j]>=pm->npart){
			nlist[n]=pm->ppaux[in][pm->p[i].j];
			n++;
		}
		if(pm->p[i].j<pm->dim-1) 
			if(pm->ppaux[in][pm->p[i].j+1]>=pm->npart){
				nlist[n]=pm->ppaux[in][pm->p[i].j+1];
				n++;
				}
	}
	if(pm->p[i].i<pm->dim-1){
		in=pm->p[i].i+1;
		if(pm->p[i].j>0) 
			if(pm->ppaux[in][pm->p[i].j-1]>=pm->npart){
				nlist[n]=pm->ppaux[in][pm->p[i].j-1];
				n++;
			}
		if(pm->ppaux[in][pm->p[i].j]>=pm->npart){
			nlist[n]=pm->ppaux[in][pm->p[i].j];
			n++;
		}
		if(pm->p[i].j<pm->dim-1) 
			if(pm->ppaux[in][pm->p[i].j+1]>=pm->npart){
				nlist[n]=pm->ppaux[in][pm->p[i].j+1];
				n++;
				}
	}
	in=pm->p[i].i;
	if(pm->p[i].j>0) 
		if(pm->ppaux[in][pm->p[i].j-1]>=pm->npart){
			nlist[n]=pm->ppaux[in][pm->p[i].j-1];
			n++;
		}
	if(pm->p[i].j<pm->dim-1) 
		if(pm->ppaux[in][pm->p[i].j+1]>=pm->npart){
			nlist[n]=pm->ppaux[in][pm->p[i].j+1];
			n++;
			}
	return n;
}


void MCStep_Neigh(MODEL *pm){ //one MC step with different dynamics
	long i,j;
	long dim,n;
	double e;
	long nlist[8];
//	double etmp;
//	double foo;



	j=pm->dim*pm->dim;
	for(i=0;i<pm->npart;i++)
		pm->ppaux[pm->p[i].i][pm->p[i].j]=i;
	for(i=pm->npart;i<j;i++)
		pm->ppaux[pm->p[i].i][pm->p[i].j]=i;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){
		n=find_neighb_holes(i,nlist,pm);//find neighbor holes
		if(n>0){
 			dim=nlist[(long)(n*drand())];
			pm->p[dim].type=(char)(drand()+.5);
			e=singeint(i,pm);
			swappart(i,dim,pm);
			e-=singeint(i,pm);
			if(!(e>=0 || drand()<=exp(e/pm->T))){
				swappart(i,dim,pm);
				pm->acpt-=1;
			}
		}
		else pm->acpt-=1;

	}
	pm->acpt/=pm->npart;
}



_inline double lupba(double e,LUPBA *pba,MODEL *pm){// returns log(weight(e))

	double dx;
	long i;

//	return(-e/pm->T);
	if(e<=pba->minx) return -pba->pb[0]*e+pba->pa[0];//-(e-pba->minx)/pm->T;
	if(e>=pba->maxx) return -pba->pb[pba->dim-1]*e+pba->pa[pba->dim-1];// -(e-pba->maxx)/pm->T;
//	if(e>pba->maxx) return -pba->pb[pba->dim-1]*pba->maxx+pba->pa[pba->dim-1];
	dx=(pba->maxx-pba->minx)/(pba->dim-1);
	i=(long) ((e-pba->minx)/dx);
	return (-pba->pb[i]*e+pba->pa[i]);

	
}

_inline double eintrem(int i,MODEL *pm){
	long dim;
	double ret;

	ret=0;
	for(dim=0;dim<i;dim++){
		ret+=pot(i,dim,pm);
//		ret+=pm->ppvjell[pm->p[dim].i][pm->p[dim].j];
	}

	for(dim=i+1;dim<pm->npart;dim++){
		ret+=pot(i,dim,pm);
//		ret+=pm->ppvjell[pm->p[dim].i][pm->p[dim].j];
	}
//	ret+=pm->ppvjell[pm->p[i].i][pm->p[i].j]*pm->npart-pm->mu;
	ret+=-pm->mu;
	return ret;
}

_inline double eintadd(int i,MODEL *pm){
	long dim;
	double ret;
	
	ret=0;
	for(dim=0;dim<pm->npart;dim++){
		ret+=pot(i,dim,pm);
//		ret+=pm->ppvjell[pm->p[dim].i][pm->p[dim].j];
	}
//	ret+=pm->ppvjell[pm->p[i].i][pm->p[i].j]*(pm->npart+1)-pm->mu;
	ret+=-pm->mu;
	return ret;
}




void MCStep_multi(MODEL *pm,LUPBA *pba){ //one multicanonical MC step  //typedef struct tagLUPBA in Cluster.h
	long i;
	long dim,n;
	double e,te;
//	double etmp;
	double foo;
	double eavg=0;
	long ecount=0;

	te=tot_eng(pm);
	n=pm->dim*pm->dim-pm->npart;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){
 		dim=pm->npart+(long)(n*drand());
		pm->p[dim].type=(char)(drand()+.5);
//		e=tot_eng(pm);
		e=singeint(i,pm);
		swappart(i,dim,pm);
//		e-=tot_eng(pm);
		e-=singeint(i,pm);

//		if(fabs(etmp/e-1)>1e-6)
//			fprintf(stderr,"%lg ",etmp/e-1);
//		if(!(e>=0 || drand()<=exp(e/pm->T))){
		foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
		if(foo<0 && drand()>exp(foo)){
			swappart(i,dim,pm);
			pm->acpt-=1;
		}
		else te-=e;
	}
	if(0!=e){
		eavg+=fabs(e);
		ecount++;
	}

	pm->pE[0]+=eavg/ecount;
	pm->acpt/=pm->npart;
}

void MCStepMu_rho(MODEL *pm,LUPBA *pw){ //one MC step with fixed chemical potential with density weight
	long i,j;
	long n,dim;
//	double r,r0;
	double e,te;
	char type;
//	double tmp;
//	double foo;
//	double eavg;
//	long ecount;
	double b=1/pm->T;
 
	te=tot_eng(pm)-pm->npart*pm->mu;
	n=pm->dim*pm->dim;
	pm->acpt=n;
//	ecount=0;
//	eavg=0;
	for(i=0,e=0;i<n;i++){
 		dim=(long)(n*drand());
		j=(long)(3*drand());
//		tmp=tot_eng(pm)-pm->npart*pm->mu;
		switch(j){ 
			case 0: //we remove a particle if exists
				if(dim<pm->npart){
					e=eintrem(dim,pm);
					if(e>=0 || drand()<=exp(e*b+pw->pb[pm->npart-1])){
						swappart(dim,--pm->npart,pm);//move accepted
						te-=e;
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
					else
						pm->acpt--;
				}
				break;
			case 1: // we add or modify a particle
			case 2:
				if(dim<pm->npart){// we try to change one particle
					e=singeint(dim,pm);
					type=pm->p[dim].type;
					pm->p[dim].type=j/2;
					if(type!=pm->p[dim].type){
						e-=singeint(dim,pm);
						if(!(e>=0 || drand()<=exp(e*b))) {
							pm->p[dim].type=type;//move not accepted
							pm->acpt--;
						}
						else{
							te-=e;
//							tmp-=tot_eng(pm)-pm->npart*pm->mu;
//							fprintf(stderr,"%lg %lg\n",e,e-tmp);
						}
					}
				}
				else if(pm->npart<n) {//we try to add a new particle
					pm->p[dim].type=j/2;
					e=-eintadd(dim,pm);
					if(e>=0 || drand()<=exp(e*b-pw->pb[pm->npart])){
						swappart(dim,pm->npart++,pm);//move accepted
						te-=e;
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
				}
				else
					pm->acpt--;
				break;
		}
//		if(0!=e){
//			eavg+=fabs(e);
//			ecount++;
//		}
	}
	pm->acpt/=n;
//	pm->pE[0]+=eavg/ecount;
}




void MCStepMu_multi(MODEL *pm,LUPBA *pba){ //one MC step with fixed chemical potential
	long i,j;
	long n,dim;
//	double r,r0;
	double e,te;
	char type;
//	double tmp;
	double foo;
	double eavg;
	long ecount;
 
	te=tot_eng(pm)-pm->npart*pm->mu;
	n=pm->dim*pm->dim;
	pm->acpt=n;
	ecount=0;
	eavg=0;
	for(i=0,e=0;i<n;i++){
 		dim=(long)(n*drand());
		j=(long)(3*drand());
//		tmp=tot_eng(pm)-pm->npart*pm->mu;
		switch(j){ //we remove a particle
			case 0:
				if(dim<pm->npart){
					e=eintrem(dim,pm);
					foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
					if(foo>=0 || drand()<=exp(foo)){
						swappart(dim,--pm->npart,pm);//move accepted
						te-=e;
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
					else
						pm->acpt--;
				}
				break;
			case 1: // we add or modify a particle
			case 2:
				if(dim<pm->npart){// we try to swap one particle
					e=singeint(dim,pm);
					type=pm->p[dim].type;
					pm->p[dim].type=j/2;
					if(type!=pm->p[dim].type){
						e-=singeint(dim,pm);
						foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
						if(!(foo>=0 || drand()<=exp(foo))){
							pm->p[dim].type=type;//move not accepted
							pm->acpt--;
						}
						else{
							te-=e;
//							tmp-=tot_eng(pm)-pm->npart*pm->mu;
//							fprintf(stderr,"%lg %lg\n",e,e-tmp);
						}
					}
				}
				else if(pm->npart<n) {//we try to add a new particle
					pm->p[dim].type=j/2;
					e=-eintadd(dim,pm);
					foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
					if(foo>=0 || drand()<=exp(foo)){
						swappart(dim,pm->npart++,pm);//move accepted
						te-=e;
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
				}
				else
					pm->acpt--;
				break;
		}
		if(0!=e){
			eavg+=fabs(e);
			ecount++;
		}
	}
	pm->acpt/=n;
	pm->pE[0]+=eavg/ecount;
}



void MCStep_multi_new(MODEL *pm,LUPBA *pba){ //one multicanonical MC step
	long i,j,k;
	long dim,n;
	double e,te;
//	double etmp;
	double foo;
#define NSWAP 3
	long lhist[2][NSWAP];
	double dhist[2][NSWAP];

	te=tot_eng(pm);
	n=pm->dim*pm->dim-pm->npart;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){

//		te=tot_eng(pm);
		e=0;
		for(j=0;j<NSWAP;j++){
 			dim=pm->npart+(long)(n*drand());
			k=(long)(pm->npart*drand());
			lhist[0][j]=dim;
			lhist[1][j]=k;
			dhist[0][j]=pm->p[dim].type;
			dhist[1][j]=pm->p[k].type;
			pm->p[dim].type=(char)(drand()+.5);
			e+=singeint(k,pm);
			swappart(k,dim,pm);
			e-=singeint(k,pm);
		}
//		e=tot_eng(pm);
//		if(fabs(etmp/e-1)>1e-6)
//			fprintf(stderr,"%lg ",etmp/e-1);
//		if(!(e>=0 || drand()<=exp(e/pm->T))){
		foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
		if(foo<0 && drand()>exp(foo)){
			for(j=NSWAP-1;j>=0;j--){
				dim=lhist[0][j];
				k=lhist[1][j];
				swappart(k,dim,pm);
				pm->p[dim].type=dhist[0][j];
				pm->p[k].type=dhist[1][j];
			}
			pm->acpt-=1;
		}
		else te-=e;
	}
	pm->acpt/=pm->npart;
}





void MCStep_alt(MODEL *pm){ //one MC step
	long i;
	long dim,n;
	double e;
//	double etmp;
//	double foo;
//	PARTICLE tmp;

	n=pm->dim*pm->dim;//-pm->npart;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){
 		dim=(long)(n*drand());
		e=singeint(i,pm);
		if (dim<pm->npart){
			pm->p[i].type=pm->p[i].type==0?1:0;
		}
		else{
			swappart(i,dim,pm);
		}
		e-=singeint(i,pm);

		if(!(e>=0 || drand()<=exp(e/pm->T))){
			if (dim<pm->npart){
				pm->p[i].type=pm->p[i].type==0?1:0;
			}
			else{
				swappart(i,dim,pm);
			}
			pm->acpt-=1;
		}
	}
	pm->acpt/=pm->npart;
}




void MCStepMu(MODEL *pm){ //one MC step with fixed chemical potential
	long i,j;
	long n,dim;
//	double r,r0;
	double e;
	char type;
//	double tmp;
 
	n=pm->dim*pm->dim;
	pm->acpt=n;
	for(i=0;i<n;i++){
 		dim=(long)(n*drand());
		j=(long)(3*drand());
//		tmp=tot_eng(pm)-pm->npart*pm->mu;
		switch(j){
			case 0:
				if(dim<pm->npart){
					e=eintrem(dim,pm);
					if(e>=0 || drand()<=exp(e/pm->T)){
						swappart(dim,--pm->npart,pm);//move accepted
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
					else
						pm->acpt--;
				}
				else
					pm->acpt--;
				break;
			case 1:
			case 2:
				if(dim<pm->npart){// we just swap one particle, no need to care about jellium
					e=singeint(dim,pm);
					type=pm->p[dim].type;
					pm->p[dim].type=j/2;
					if(type!=pm->p[dim].type){
						e-=singeint(dim,pm);
						if(!(e>=0 || drand()<=exp(e/pm->T))) {
							pm->p[dim].type=type;//move not accepted
							pm->acpt--;
						}
//						else{
//							tmp-=tot_eng(pm)-pm->npart*pm->mu;
//							fprintf(stderr,"%lg %lg\n",e,e-tmp);
//						}
					}
					else
						pm->acpt--;

				}
				else if(pm->npart<n) {//we add a new particle
					pm->p[dim].type=j/2;
					e=-eintadd(dim,pm);
					if(e>=0 || drand()<=exp(e/pm->T)){
						swappart(dim,pm->npart++,pm);//move accepted
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
				}
				else
					pm->acpt--;
				break;
		}
	}
	pm->acpt/=n;
}



_inline  int is_in(int kmin,int kmax,int i){
	return (kmin<i)&&(kmax>i);
}



void rrcor(int mode,double part, MODEL *pm){ //density-density correlation
	long i,j;
	int min,max;

	if(1== mode){
		for(i=-pm->dim;i<=pm->dim;i++)
			for(j=-pm->dim;j<=pm->dim;j++)
				pm->pc11[i][j]=0;
		return;
	}

	if(1==part)
		for(i=0;i<pm->npart;i++)
			for(j=0;j<pm->npart;j++){
				pm->pc11[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;

			}
	else {
		min=(int)(pm->dim*(.5-part/2.));
		max=(int)(pm->dim*(.5+part/2.));
		for(i=0;i<pm->npart;i++)
			for(j=0;j<pm->npart;j++){
				if(is_in(min,max,pm->p[i].i)&&is_in(min,max,pm->p[i].j)&&is_in(min,max,pm->p[j].i)&&is_in(min,max,pm->p[j].j))
					pm->pc11[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;

			}
	}
}



void cor_per( MODEL *pm){ //modifies correlation matrix for the periodic case
	long i,j;
	double **pptmp;
	double **pp;
	
#define sign(x) ((x)<0?-1:((x)==0?0:1))

	pptmp=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);

	pp=pm->pc11;
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pptmp[i][j]=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pp[i][j]=pptmp[i][j];

	pp=pm->pc22;
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pptmp[i][j]=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pp[i][j]=pptmp[i][j];


	pp=pm->pc12;
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pptmp[i][j]=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pp[i][j]=pptmp[i][j];

	pp=pm->pc21;
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pptmp[i][j]=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pp[i][j]=pptmp[i][j];

	free_dmatrix(pptmp,-pm->dim,pm->dim,-pm->dim,pm->dim);
#undef sign
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////



void gencor( MODEL *pm){ //calculates all four possible correlations. //(up-up,up-down, down-down, down-up).Joa
	long i,j;

	for(i=0;i<pm->npart;i++)
		if(0==pm->p[i].type)
			for(j=0;j<pm->npart;j++)
				if(0==pm->p[j].type)
					pm->pc11[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;
				else
					pm->pc12[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;
		else
			for(j=0;j<pm->npart;j++)
				if(0==pm->p[j].type)
					pm->pc21[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;
				else
					pm->pc22[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;
	pm->ncor++;
}

double getcorr_per(int i, int j,int type,MODEL *pm){//retrives specific correlation 
	double cor;
	double rhoavg=pm->npart/((double)pm->dim*pm->dim);
	double d11,d22,d12,d21;
	double **pp;
	if(pm->ncor==0) return 0;

#define sign(x) ((x)<0?-1:((x)==0?0:1))

	pp=pm->pc11;
	d11=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	pp=pm->pc22;
	d22=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	pp=pm->pc12;
	d12=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	pp=pm->pc21;
	d21=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];


#undef sign

	switch(type){
	case 0:
	default:
		cor=d11;
		cor+=d22;
		cor+=d12;
		cor+=d21;
		cor-=pm->dim*pm->dim*rhoavg*rhoavg*pm->ncor;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0])*(1-rhoavg); // 13.11.05 I hope that this is correct now.
//		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]-(pm->dim)*(pm->dim)*rhoavg*rhoavg*pm->ncor);
//		cor/=pm->ncor;
		break;
	case 11:
		cor=d11;
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 22:
		cor=d22;
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 12:
		cor=d12;
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 21:
		cor=d21;
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 1: //spin-spin correlation
		cor=d11;
		cor+=d22;
		cor-=d12;
		cor-=d21;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	}
	return cor;
}



double getcorr_open(int ix, int iy,int type,MODEL *pm){//retrives specific correlation for open BC
	double cor;
	double rhoavg=pm->npart/((double)pm->dim*pm->dim);

	if(pm->ncor==0) return 0;
	switch(type){
	case 0:
	default:
		cor=pm->pc11[ix][iy];
		cor+=pm->pc22[ix][iy];
		cor+=pm->pc12[ix][iy];
		cor+=pm->pc21[ix][iy];
		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0])*(1-rhoavg); // 13.11.05 I hope that this is correct now.
//		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]-(pm->dim)*(pm->dim)*rhoavg*rhoavg*pm->ncor);
//		cor/=pm->ncor;
		break;
	case 11:
		cor=pm->pc11[ix][iy];
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 22:
		cor=pm->pc22[ix][iy];
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 12:
		cor=pm->pc12[ix][iy];
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 21:
		cor=pm->pc21[ix][iy];
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 1: //spin-spin correlation
		cor=pm->pc11[ix][iy];
		cor+=pm->pc22[ix][iy];
		cor-=pm->pc12[ix][iy];
		cor-=pm->pc21[ix][iy];
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	}
	return cor;
}

double getcorr(int ix, int iy,int type,MODEL *pm){//retrives specific correlation 
	switch(pm->pottype){
		default:
			return getcorr_open(ix,iy,type,pm);
			break;
		case 3:
		case 4:
			return getcorr_per(ix,iy,type,pm);
			break;
	}
}


void cleancor(MODEL *pm){ //clean all four correlation arrays
	long i,j;

		for(i=-pm->dim;i<=pm->dim;i++)
			for(j=-pm->dim;j<=pm->dim;j++)
			{
				pm->pc11[i][j]=0;
				pm->pc22[i][j]=0;
				pm->pc12[i][j]=0;
				pm->pc21[i][j]=0;
			}
		pm->ncor=0;
}


long FFTNextPowerOfTwo(long n)
{

		n=1<<(long)ceil(log((double)n)/log(2.));
		return n;
}


void four_trans(MODEL *pm){
	long i,j;
	double ***tdata,**tspeq;
	double tmp,tmp1;//,sum;
	long limit;

	limit=pm->ftdim*2;
//	return;
	tdata=f3tensor(1,1,1,limit,1,limit);
	tspeq=dmatrix(1,1,1,limit*2);
//	tmp=-(double)pm->npart/(pm->dim*pm->dim-pm->npart);
	tmp=0;
	for(i=1;i<=limit;i++)
		for(j=1;j<=limit;j++) 
		{
			if(j<=pm->dim && i<=pm->dim){
//				tdata[1][i][j]=1-cos(3*(i+j));
				tdata[1][i][j]=tmp;
			}
			else
				tdata[1][i][j]=0;
		}
	for(i=0;i<pm->npart;i++){
		tdata[1][pm->p[i].i+1][pm->p[i].j+1]=1;
	}
	for(i=1;i<=pm->dim;i++){//apodization
		tmp1=cos(PI * (i-pm->dim/2.)/pm->dim);
		tmp1*=tmp1;
		for(j=1;j<=pm->dim;j++)
		{
			tmp=cos(PI*(j-pm->dim/2.)/pm->dim);
			tmp*=tmp;
			tdata[1][i][j]*=tmp*tmp1; 
		}
	}
	rlft3(tdata,tspeq,1,limit,limit,1);
	for(i=1;i<=pm->ftdim;i++) //unscramble FFT
		for(j=1;j<=pm->ftdim;j++){
			tmp=tdata[1][i][2*j-1]*tdata[1][i][2*j-1];
			tmp+=tdata[1][i][2*j]*tdata[1][i][2*j];
			pm->ppft[i-1][j-1]=sqrt(tmp);
			tmp=tdata[1][limit-i+1][2*j-1]*tdata[1][limit-i+1][2*j-1];
			tmp+=tdata[1][limit-i+1][2*j]*tdata[1][limit-i+1][2*j]; 
			pm->ppft[-i][j-1]=sqrt(tmp);
		}

//	pm->ppft[0][0]=0;	
	free_dmatrix(tspeq,1,1,1,limit*2);
	free_f3tensor(tdata,1,1,1,limit,1,limit);
}




void four_trans_spin(MODEL *pm){
	long i,j,ir,jr;
	double ***tdata,**tspeq;
	double tmp,tmp1;//,sum;
	long limit;

	limit=pm->ftdim*2;
//	return;
	tdata=f3tensor(1,1,1,limit,1,limit);
	tspeq=dmatrix(1,1,1,limit*2);
//	tmp=-(double)pm->npart/(pm->dim*pm->dim-pm->npart);
	tmp=0;
	for(i=1;i<=limit;i++)
		for(j=1;j<=limit;j++) 
		{
			ir=i-pm->ftdim;
			jr=j-pm->ftdim;
			if(jr<=pm->dim && ir<=pm->dim && jr>=-pm->dim && ir>=-pm->dim){
//				tdata[1][i][j]=1-cos(3*(i+j));
				tdata[1][i][j]=getcorr(i-pm->ftdim,j-pm->ftdim,1,pm);
			}
			else
				tdata[1][i][j]=0;
		}
	for(i=1;i<=limit;i++){//apodization
		tmp1=cos(PI * (i-limit/2)/limit);
		tmp1*=tmp1;
		for(j=1;j<=limit;j++)
		{
			tmp=cos(PI*(j-limit/2.)/limit);
			tmp*=tmp;
			tdata[1][i][j]*=tmp*tmp1; 
		}
	}
	rlft3(tdata,tspeq,1,limit,limit,1);
	for(i=1;i<=pm->ftdim;i++) //unscramble FFT
		for(j=1;j<=pm->ftdim;j++){
			tmp=tdata[1][i][2*j-1]*tdata[1][i][2*j-1];
			tmp+=tdata[1][i][2*j]*tdata[1][i][2*j];
			pm->ppft[i-1][j-1]=sqrt(tmp);
			tmp=tdata[1][limit-i+1][2*j-1]*tdata[1][limit-i+1][2*j-1];
			tmp+=tdata[1][limit-i+1][2*j]*tdata[1][limit-i+1][2*j]; 
			pm->ppft[-i][j-1]=sqrt(tmp);
		}

//	pm->ppft[0][0]=0;	
	free_dmatrix(tspeq,1,1,1,limit*2);
	free_f3tensor(tdata,1,1,1,limit,1,limit);
}


double calcVL(MODEL *pm){//calculation of the cumulant VL (see Binder, p61)
	long i;
	double E1,E2,E4,Etmp;

	E1=0;
	E2=0;
	E4=0;
	for(i=pm->nstartMC+1;i<=pm->nstartMC+pm->nMC;i++){
		E1+=pm->pE[i];
	}
	E1/=pm->nMC;
	for(i=pm->nstartMC+1;i<=pm->nstartMC+pm->nMC;i++){
		Etmp=pm->pE[i]-E1;
		Etmp*=Etmp;
		E2+=Etmp;
		E4+=Etmp*Etmp;
	}
	E2/=pm->nMC;
	E4/=pm->nMC;
	return 1.-E4/3./E2/E2;

}

double calcsigrel(MODEL *pm){
	long i,iMC;
	double E2,E,xi,Exi,Etmp;

	E=0;
	E2=0;
	xi=0;
	Exi=0;
	for(iMC=pm->nstartMC+1,i=0;i<pm->nMC;i++,iMC++){
		Etmp=pm->pE[iMC];
		E+=Etmp;
		Exi+=i*Etmp;
		Etmp*=Etmp;
		E2+=Etmp;
	}
	E/=pm->nMC;
	E2/=pm->nMC;
	E2=sqrt(E2-E*E);// SigmaE
	Exi/=pm->nMC;// Avg(E*i)
	Exi-=E*(pm->nMC-1)/2;//Avg(E*i)-Avg(E)*Avg(i)
	Exi=pm->nMC/(pm->nMC-1)*Exi;//cov(i,E)
	Exi=Exi/E2/sqrt((pm->nMC*pm->nMC-1.)/12);//r(i,E)=cov(i,E)/Sig(E)/Sig(i)
	return sqrt(1.-Exi*Exi);

}

int count(MODEL *pm,int i,int j);

int count(MODEL *pm,int i,int j){//recursive auxilary counting function. It counts the number of neighbors which are of the 
	int ret=1;																			//same kind(either hole or particle).Joa

	if(0==pm->ppaux[i][j]) return 0;//stop counting.Joa
	pm->ppaux[i][j]=0;//stop counting for the new site in the next iteraction.Joa
	if(i>0) ret+=count(pm,i-1,j);
	if(i<pm->dim-1) ret+=count(pm,i+1,j);
	if(j>0) ret+=count(pm,i,j-1);
	if(j<pm->dim-1) ret+=count(pm,i,j+1);
	return ret;
}


int count_per(MODEL *pm,int i,int j){//recursive auxilary counting function periodic boundaries
	int ret=1;

	if(0==pm->ppaux[i][j]) return 0;//stop counting.Joa
	pm->ppaux[i][j]=0;//stop counting for this site in the next iteraction.Joa
	ret+=count(pm,(i-1+pm->dim)%pm->dim,j);// ??? % module operator:
	ret+=count(pm,(i+1+pm->dim)%pm->dim,j);//e1 � (e1 / e2) * e2, where both operands are of integral types. Joa
	ret+=count(pm,i,(j-1+pm->dim)%pm->dim);
	ret+=count(pm,i,(j+1+pm->dim)%pm->dim);
	return ret;
}


void clustcorr( MODEL *pm, int k, int l){		//calculates particle correlations among diferent clusters. Joa
	long i,x,y;										//k and l are centered particle coordinates. 
	double rad1;
	int rad;
	for(i=0;i<pm->npart;i++){
		x=pm->p[i].i-k;
		y=pm->p[i].j-l;
		rad1=sqrt(x*x+y*y);    
		rad=rad1;
		if((-1!=pm->ppaux[pm->p[i].i][pm->p[i].j]) && (rad1<10))
				pm->pc11[x][y]++;		//It goes to initialize(&ret) in file.c. After that inside file.c it is read it and write it each pc11 with hr=bread_dmat(ret.pc11,-ret.dim,ret.dim,-ret.dim,ret.dim,fp)
				//pm->dist[rad]++;		//preparing histogram for number of neighbors at distance rad. Joa
				pm->ncor++;
	}
}


int mark(MODEL *pm,int i,int j){						//recursive auxilary counting function. It marks rim cluster and measures perimeter 
	int ret=1;											//same kind(either hole or particle).Joa
	if((0==pm->ppaux[i][j])||(-1==pm->ppaux[i][j])) {
		-1==pm->ppaux[i][j];
		return 0;//stop counting.Joa
}
	pm->ppaux[i][j]=0;//stop counting for the new site in the next iteraction.Joa
	if(i>0) ret+=count(pm,i-1,j);
	if(i<pm->dim-1) ret+=count(pm,i+1,j);
	if(j>0) ret+=count(pm,i,j-1);
	if(j<pm->dim-1) ret+=count(pm,i,j+1);
	return ret;
}


void genclustcorr(MODEL* pm){//Takes centered particle or hole that's surrended by others. called from Main in main.Rutine introduced by Joa
	long i,j,k;//,k=0;
	long part=1,hole=0;

	j=pm->dim*pm->dim;
	if(pm->npart>j/2.) //When filling > 0.5 we count hole-clusters
	{
		hole=1;
		part=0;
	}
	memset(pm->paux1+1,0,pm->npart*sizeof(*(pm->paux1)));//?? writes on pm->paux1+1,a string of size npart*sizeof(*(pm->paux1).Joa
	for(i=0;i<pm->npart;i++)		//particle condition
		pm->ppaux[pm->p[i].i][pm->p[i].j]=part;
	for(i=pm->npart;i<j;i++)		//hole condition
		pm->ppaux[pm->p[i].i][pm->p[i].j]=hole;
	for(i=0;i<j;i++){				//For all sites
		if(1==pm->ppaux[pm->p[i].i][pm->p[i].j]){		
				k=mark(pm,pm->p[i].i,pm->p[i].j);    
				clustcorr(pm, pm->p[i].i,pm->p[i].j);
		}
	}
}

void count_clusters(MODEL* pm){//counts cluster sizes. called from count_clusters(&mod) in main.Joa
	long i,j,k;//,k=0;
	long part=1,hole=0;

	j=pm->dim*pm->dim;
	if(pm->npart>j/2.) //When filling > 0.5 we count hole-clusters
	{
		hole=1;
		part=0;
	}
	memset(pm->paux1+1,0,pm->npart*sizeof(*(pm->paux1)));//?? writes on pm->paux1+1,a string of size npart*sizeof(*(pm->paux1).Joa
	//memset(pm->pauxl+1,0,pm->npart*sizeof(*(pm->pauxl)));
	for(i=0;i<pm->npart;i++)		//particle condition
		pm->ppaux[pm->p[i].i][pm->p[i].j]=part;
	for(i=pm->npart;i<j;i++)		//hole condition
		pm->ppaux[pm->p[i].i][pm->p[i].j]=hole;
	for(i=0;i<j;i++){				//For all sites
		if(1==pm->ppaux[pm->p[i].i][pm->p[i].j]){	
			switch(pm->pottype)
			{
				default:
					k=count(pm,pm->p[i].i,pm->p[i].j);    //e.g k=9. Joa
					break;
				case 3:
				case 4:
					k=count_per(pm,pm->p[i].i,pm->p[i].j);
					break;
			}
//			k+=j;
			pm->paux1[k]++;//Ex. for i1 and j1 paux1[9], it might happen that i2 and j2 also have paux1[6], but now paux1[6] will increase one. it will point to the next elemt?Joa
			//pm->pauxl[dl]++;
		}
	}
	for(i=1;i<=pm->npart;i++){
		pm->phist[i]+=pm->paux1[i];//??
		pm->phist2[i]+=pm->paux1[i]*(double)pm->paux1[i];
		//pm->phistl[i]+=pm->pauxl[i];
	}
	pm->nClHist++;//For each Montecarlo Step? Joa

}

double  hole_pair(MODEL *pm, int pc1,int pc2,int pc3,int pc4,int kk,int ll){	
	double conduct1=0;
	int count1=0;
//	double tmp=0;
	int ext=11;
	if((kk<pm->dim+pm->dim-1-ext)&(pm->npart<pm->ppaux[kk+1][ll])&(2501!=pm->ppaux[kk+1][ll])){
		conduct1+= mobility(pm,pc1,pc2,pc3,pc4,kk,kk+1,ll,ll);
//		count1+=1;
	}
	if((kk>-pm->dim + ext)&(pm->npart<pm->ppaux[kk-1][ll])&(2501!=pm->ppaux[kk-1][ll])){
		conduct1+= mobility(pm,pc1,pc2,pc3,pc4,kk,kk-1,ll,ll);
//	    count1+=1;
	}
	if((ll<pm->dim+pm->dim-1-ext)&(pm->npart<pm->ppaux[kk][ll+1])&(2501!=pm->ppaux[kk][ll+1])){
		conduct1+= mobility(pm,pc1,pc2,pc3,pc4,kk,kk,ll,ll+1);
//		count1+=1;
	}
	if((ll>-pm->dim + ext)&(pm->npart<pm->ppaux[kk][ll-1])&(2501!=pm->ppaux[kk][ll-1])){
		conduct1+= mobility(pm,pc1,pc2,pc3,pc4,kk,kk,ll,ll-1);
//		count1+=1;
	}
	if (count1=!0) return conduct1/=count1;
	else return conduct1;
}

_inline double mobility1(MODEL *pm,long i,long j,double dr, double dx){
	double en_h,en_p,delta_e;
	double alf=1.0;
	double tmpexp=0;
	double invT=1/pm->T;

		en_p=singeint(i,pm);
		pm->p[j].type=pm->p[i].type;
		swappart(i,j,pm);	
		en_h=singeint(i,pm);
		swappart(i,j,pm);
		if(en_h>en_p){
		    delta_e=en_h-en_p;
			tmpexp=exp(-alf*dr-delta_e*invT);
			tmpexp*=invT*dx*dx;
		}
return tmpexp;
}

_inline int distance2(int x2,int x1,int y2,int y1){
	int r2,dx,dy;
	dx=x2-x1;
	dy=y2-y1;
	r2=dx*dx+dy*dy;
	return r2;
}

_inline double distance(double x2,double x1,double y2,double y1){
	double r,dx,dy;
	dx=x2-x1;
	dy=y2-y1;
	r=dx*dx+dy*dy;
	r=sqrt(r);
	return r;
}

_inline double mobility(MODEL *pm,long p1,long p2,long h1,long h2, double r, double dx,double en_p){
	double en_h,delta_e,en_int;
	double alf=1.0;
	double tmpexp=0;
	double invT=1./pm->T;

	pm->p[h1].type=pm->p[p1].type;
	pm->p[h2].type=pm->p[p2].type;
	swappart(p1,h1,pm);
	swappart(p2,h2,pm);
	en_h=singeint(p1,pm);
	en_h+=singeint(p2,pm);
	en_int=pot(p1,p2,pm);
	en_h-=en_int;
	swappart(p1,h1,pm);
	swappart(p2,h2,pm);
		if(en_h>en_p){
		    delta_e=en_h-en_p;
		    tmpexp=exp(-alf*r-delta_e*invT);
			tmpexp*=invT*dx*dx;
		}
return tmpexp;
}

double bipolaroncond(MODEL *pm, long kk){
	int i,j,k,l,m,s,r2;
	double dx,xp1,yp1,xp2,yp2,xp,xh,yp,yh,xcm,ycm,ll,r;
	double en_p;
	double cond2=0.0;
	double rc=6.0;
	double dop;
	char spi,spj;
	k=0;
	s=pm->dim*pm->dim;
	ll=pm->dim;
	dop=pm->npart;
	dop/=s;

	for(i=0;i<pm->npart;i++){
		for(j=i+1;j<pm->npart;j++){ 
				xp1=pm->p[i].i;
				yp1=pm->p[i].j;
				xp2=pm->p[j].i;
				yp2=pm->p[j].j;
				spi=pm->p[i].type;
				spj=pm->p[j].type;
				r2=distance2(xp2,xp1,yp2,yp1);
				if((r2==1)&(spi==spj)){//&(spi==spj)
					k++;
					xcm=(xp2+xp1)/2;
					ycm=(yp2+yp1)/2;
					pm->bip[k].x=xcm;
					pm->bip[k].y=ycm;
					pm->bip[k].p1=i;
					pm->bip[k].p2=j;
				}
				else if ((abs(xp2-xp1)==pm->dim-1)&(yp2==yp1)&(spi==spj)){//&(spi==spj)
					k++;
					xcm=pm->dim+1/2;
					ycm=(yp2+yp1)/2;
					pm->bip[k].x=xcm;
					pm->bip[k].y=ycm;
					pm->bip[k].p1=i;
					pm->bip[k].p2=j;
				}
				else if ((abs(yp2-yp1)==pm->dim-1)&(xp2==xp1)&(spi==spj)){//&(spi==spj
					k++;
					ycm=pm->dim+1/2;
					xcm=(xp2+xp1)/2;
					pm->bip[k].x=xcm;
					pm->bip[k].y=ycm;
					pm->bip[k].p1=i;
					pm->bip[k].p2=j;
				}
		}
	}
	l=k;
	for(i=pm->npart;i<s;i++){
		for(j=i+1;j<s;j++){
				xp1=pm->p[i].i;
				yp1=pm->p[i].j;
				xp2=pm->p[j].i;
				yp2=pm->p[j].j;
				r2=distance2(xp2,xp1,yp2,yp1);
				if(r2==1){//&(spi==spj)
					l++;
					xcm=(xp2+xp1)/2;
					ycm=(yp2+yp1)/2;
					pm->bip[l].x=xcm;
					pm->bip[l].y=ycm;
					pm->bip[l].p1=i;
					pm->bip[l].p2=j;
				}
				else if ((abs(xp2-xp1)==(pm->dim-1))&(yp2==yp1)){//&(spi==spj)
					l++;
					xcm=pm->dim+1/2;
					ycm=(yp2+yp1)/2;
					pm->bip[l].x=xcm;
					pm->bip[l].y=ycm;
					pm->bip[l].p1=i;
					pm->bip[l].p2=j;
				}
				else if ((abs(yp2-yp1)==(pm->dim-1))&(xp2==xp1)){//&(spi==spj)
					l++;
					ycm=pm->dim+1/2;
					xcm=(xp2+xp1)/2;
					pm->bip[l].x=xcm;
					pm->bip[l].y=ycm;
					pm->bip[l].p1=i;
					pm->bip[l].p2=j;
				}
		}
	}
			
	for(i=1;i<=k;i++){
			en_p=singeint(pm->bip[i].p1,pm)+singeint(pm->bip[i].p2,pm)-pot(pm->bip[i].p1,pm->bip[i].p2,pm);
			xp=pm->bip[i].x;
			yp=pm->bip[i].y;
			for(j=k+1;j<=l;j++){
				xh=pm->bip[j].x;
				yh=pm->bip[j].y;
				r=distance(xp,xh,yp,yh);
				if(rc>r){
					dx=xh-xp;
					cond2+=mobility(pm,pm->bip[i].p1,pm->bip[i].p2,pm->bip[j].p1,pm->bip[j].p2,r,dx,en_p); 
				}
				yh+=pm->dim;
				r=distance(xp,xh,yp,yh);
			    if(rc>r){
					dx=xh-xp;
					cond2+=mobility(pm,pm->bip[i].p1,pm->bip[i].p2,pm->bip[j].p1,pm->bip[j].p2,r,dx,en_p);  
				}
				xh+=pm->dim;
				r=distance(xp,xh,yp,yh);
				if(rc>r){
					dx=xh-xp;
					cond2+=mobility(pm,pm->bip[i].p1,pm->bip[i].p2,pm->bip[j].p1,pm->bip[j].p2,r,dx,en_p); 
				}
				yh-=pm->dim;
				r=distance(xp,xh,yp,yh);
				if(rc>r){
					dx=xh-xp;
					cond2+=mobility(pm,pm->bip[i].p1,pm->bip[i].p2,pm->bip[j].p1,pm->bip[j].p2,r,dx,en_p); 
				}
				yh-=pm->dim;
				r=distance(xp,xh,yp,yh);
				if(rc>r){
					dx=xh-xp;
					cond2+=mobility(pm,pm->bip[i].p1,pm->bip[i].p2,pm->bip[j].p1,pm->bip[j].p2,r,dx,en_p); 
				}
				xh-=pm->dim;
				r=distance(xp,xh,yp,yh);
				if(rc>r){
					dx=xh-xp;
					cond2+=mobility(pm,pm->bip[i].p1,pm->bip[i].p2,pm->bip[j].p1,pm->bip[j].p2,r,dx,en_p);  
				}
				xh-=pm->dim;
				r=distance(xp,xh,yp,yh);
				if(rc>r){
					dx=xh-xp;
					cond2+=mobility(pm,pm->bip[i].p1,pm->bip[i].p2,pm->bip[j].p1,pm->bip[j].p2,r,dx,en_p);  
				}
				yh+=pm->dim;
				r=distance(xp,xh,yp,yh);
				if(rc>r){
					dx=xh-xp;
					cond2+=mobility(pm,pm->bip[i].p1,pm->bip[i].p2,pm->bip[j].p1,pm->bip[j].p2,r,dx,en_p); 
				}
				yh+=pm->dim;
				r=distance(xp,xh,yp,yh);
				if(rc>r){
					dx=xh-xp;
					cond2+=mobility(pm,pm->bip[i].p1,pm->bip[i].p2,pm->bip[j].p1,pm->bip[j].p2,r,dx,en_p);  
				}
			}
	}
	if(dop>0.5) cond2/=dop*s;
	else cond2/=pm->npart;
	return cond2;
}

double polaroncond(MODEL *pm, long kk){
	int i,j,s,ll;
	double xp,xh,yp,yh,x,y,r;
	double cond1=0.0;
	double mob1=0.0;
	double rc=6.0;
	double invT=1/pm->T;
	double dop;
	s=pm->dim*pm->dim;
	ll=pm->dim;
	dop=pm->npart;
	dop/=s;

	for(i=0;i<pm->npart;i++){
		for(j=pm->npart;j<s;j++){ //single particle mobility 052409
			xh=pm->p[j].i;
			xp=pm->p[i].i;
			yh=pm->p[j].j;
			yp=pm->p[i].j;
			r=distance(xp,xh,yp,yh);
			if(r<rc){//center zone
				x=xh-xp;
				mob1=mobility1(pm,i,j,r,x);
				cond1+=mob1;}
			yh+=ll; //Replica 1
			r=distance(xp,xh,yp,yh);
			if(r<rc){		
				x=xh-xp;
				mob1=mobility1(pm,i,j,r,x);
				cond1+=mob1;}
			xh+=ll; //Replica 2
			r=distance(xp,xh,yp,yh);
			if(r<rc){		
				x=xh-xp;
				mob1=mobility1(pm,i,j,r,x);
				cond1+=mob1;}
			yh-=ll; //Replica 3
			r=distance(xp,xh,yp,yh);
			if(r<rc){		
				x=xh-xp;
				mob1=mobility1(pm,i,j,r,x);
				cond1+=mob1;}
			yh-=ll; //Replica 4
			r=distance(xp,xh,yp,yh);
			if(r<rc){		
				x=xh-xp;
				mob1=mobility1(pm,i,j,r,x);
				cond1+=mob1;}	
			yh-=ll; //Replica 5
			r=distance(xp,xh,yp,yh);
			if(r<rc){		
				x=xh-xp;
				mob1=mobility1(pm,i,j,r,x);
				cond1+=mob1;}
			xh-=ll; //Replica 6
			r=distance(xp,xh,yp,yh);
			if(r<rc){		
				x=xh-xp;
				mob1=mobility1(pm,i,j,r,x);
				cond1+=mob1;}		 
			yh+=ll; //Replica 7
		    r=distance(xp,xh,yp,yh);
			if(r<rc){		
				x=xh-xp;
				mob1=mobility1(pm,i,j,r,x);
				cond1+=mob1;}
			yh+=ll;//Replica 8
            r=distance(xp,xh,yp,yh);
			if(r<rc){		
				x=xh-xp;
				mob1=mobility1(pm,i,j,r,x);
				cond1+=mob1;}
		}
	}

	if(dop>0.5) cond1/=dop*s;
	else cond1/=pm->npart;
	return cond1;
}

void cleanhist(MODEL *pm){//cleans the cluster-sizes histogram
	long i;
	for(i=1;i<=pm->npart;i++){
		pm->phist[i]=0;
		pm->phist2[i]=0;
		//pm->phistl[i]=0;
	}
//	pm->nClHist=0;
}


void cleanrhohist(MODEL *pm){//cleans the rho record
	long i;
	for(i=0;i<=pm->nMC+pm->nstartMC;i++){
		pm->phist[i]=0;
		//pm->phistl[i]=0;
	}
//	pm->nClHist=0;
}
