#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <string.h>
#include "cluster.h"
#include "nrutil.h"

FILE* fplog=0;										 //See filecreation.cpp. Joa

/*parread: rutine for reading parameters from par.par*/

PARAM parread(char *fname){ //called from main.c par=parread(argv[1]). Joa
	FILE *fp;
	PARAM ret;		// ret is used to have access to items from 'fname' when fp=fopen(fname,"r"). Joa
	long hr;
	int err=0;
	int line=1;

	
	fp=fopen(fname,"r");	//creation of file with pointer fname//char fname[1024]; element of class mid
	if(NULL==fp){
		logprintf("Error opening parameter file: %s",fname);
		ret.dim=0;
		return ret;
	}
	hr=fscanf(fp," %*s ; ");//Reads data from the current position of stream (fp). Joa 
	line++;
	hr=fscanf(fp,"dim %d ; ",&ret.dim);//reading parameters from fp. fp pointer to the file with data, this is fname. "
	line++;
	hr=fscanf(fp,"filling %lg ; ",&ret.filling);//reading from par the fp, the element filling from ret. Joa
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"pottype %d ; ",&ret.pottype);//Passed value to call in main.c, ret.pottype=par.pottype
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"V10 %lg ; ",&ret.g2);
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"V11 %lg ; ",&ret.range);
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"V20 %lg ; ",&ret.anizo);
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"init %d %lg; ",&ret.init,&ret.mu);
	if(2!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"temperature %lg ; ",&ret.T);
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"startMCsteps %ld ; ",&ret.nstartMC);
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"MCsteps %ld %lg %lg %ld; ",&ret.nMC,&ret.T0,&ret.Ea,&ret.nMC0);
	if(4!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"iterate %s %lg %lg %lg %d %lg; ",ret.ipar,&ret.istart,&ret.istop,&ret.istep,&ret.iflag,&ret.minstep);
	if(6!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"clusters %d ; ",&ret.clflag);
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"correlation %d ; ",&ret.corrflag);
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"conduction %d ; ",&ret.condflag);
	if(1!=hr) goto parread_err;
	line++;
	hr=fscanf(fp,"fname %s ; ",ret.fname);
	if(1!=hr) goto parread_err;
	line++;
	fclose(fp); 
	return ret;
parread_err:
	fclose(fp); 
	logprintf("Error parsing parameter file: '%s' near line: %d",fname,line);
	ret.dim=0;
	return ret;
}

int bwrite_dmat(double **pmat,int i1,int i2,int j1,int j2,FILE* fp){
	long i,hr;

	for(i=i1;i<=i2;i++)
		hr=fwrite(&pmat[i][j1],sizeof(double),j2-j1+1,fp);
		if(j2-j1+1!=hr){
			return 1;
		}
	return 0;

}

int bread_dmat(double **pmat,int i1,int i2,int j1,int j2,FILE* fp){
	long i,hr;

	for(i=i1;i<=i2;i++)
		hr=fread(&pmat[i][j1],sizeof(double),j2-j1+1,fp);
		if(j2-j1+1!=hr){
			hr=feof(fp);
			i=ferror(fp);
			return 1;
		}
	return 0;

} 
/*Rutine for reading parameters from par.par file. Joa */

MODEL binread(char * fname){
	FILE* fp;
	MODEL ret;//,tmp;
	long hr;
	char magic[4];//??? Joa
	static char serr[1024]={""};
	int i,j,flg;
	long m,k;

	
	ret.dim=0;
	fp=fopen(fname,"rb");
	if(NULL==fp){
		sprintf(serr,"Can not open file!: '%s'\n",fname);
		goto binread_err;
	}
	hr=fread(magic,sizeof(char),4,fp);
	if(4!=hr) goto binread_err;
	if(0==strncmp(MAGIC97,magic,4)){
		hr=fread(&ret,sizeof(MODEL97),1,fp);//Read from fp and stores in &ret, one item 1, with size sizeof(mode97), 
		if(1!=hr) goto binread_err;
		ret.acpt=-1;
		ret.pottype=0;
		ret.nClHist=0;
	} else if(0==strncmp(MAGIC98,magic,4)){
		hr=fread(&ret,sizeof(MODEL98),1,fp);
		if(1!=hr) goto binread_err;
		ret.nClHist=0;
	} else if (0==strncmp(MAGIC99,magic,4)){
		hr=fread(&ret,sizeof(MODEL99),1,fp);
		if(1!=hr) goto binread_err;
		ret.phist2=0;
	} else if (0==strncmp(MAGIC,magic,4)){
		hr=fread(&ret,sizeof(MODEL),1,fp);
		if(1!=hr) goto binread_err;
	}
	else{
		sprintf(serr,"Wrong file type!: '%s'\n",fname);
		goto binread_err;
	}
//	ret=tmp;
	initialize(&ret);
	hr=fread(ret.p,sizeof(PARTICLE),ret.npart,fp);
	if(ret.npart!=hr){
		goto binread_err;
	}
	hr=fread(&ret.pE[0],sizeof(double),ret.nstartMC+ret.nMC+1,fp);
	if((ret.nstartMC+ret.nMC+1)!=hr){
		goto binread_err;
	}
	hr=bread_dmat(ret.pc11,-ret.dim,ret.dim,-ret.dim,ret.dim,fp);
	if(0!=hr){
		goto binread_err;
	}
	hr=bread_dmat(ret.pc22,-ret.dim,ret.dim,-ret.dim,ret.dim,fp);
	if(0!=hr){
		goto binread_err;
	}
	hr=bread_dmat(ret.pc12,-ret.dim,ret.dim,-ret.dim,ret.dim,fp);
	if(0!=hr){
		goto binread_err;
	}
	hr=bread_dmat(ret.pc21,-ret.dim,ret.dim,-ret.dim,ret.dim,fp);
	if(0!=hr){
		goto binread_err;
	}
	if(0==strncmp(MAGIC,magic,4)){
		switch(ret.init){
			default:	
				hr=fread(&ret.phist[0],sizeof(ret.phist[0]),ret.npart,fp);
				hr=fread(&ret.phist2[0],sizeof(ret.phist2[0]),ret.npart,fp);
				//hr=fread(&ret.phistl[0],sizeof(ret.phistl[0]),ret.npart,fp);
				if((ret.npart)!=hr){
					goto binread_err;
				}
				break;
			case 2:
			case 20:
			case 21:
			case 22:
				hr=fread(&ret.phist[0],sizeof(ret.phist[0]),ret.nstartMC+ret.nMC+1,fp);
				//hr=fread(&ret.phistl[0],sizeof(ret.phistl[0]),ret.nstartMC+ret.nMC+1,fp);
				if((ret.nstartMC+ret.nMC+1)!=hr){
					goto binread_err;
				}
				break;
		}
	} else if(0==strncmp(MAGIC99,magic,4)){//Joa
		hr=fread(&ret.phist[0],sizeof(ret.phist[0]),ret.npart,fp);
		//hr=fread(&ret.phistl[0],sizeof(ret.phistl[0]),ret.npart,fp);
		if((ret.npart)!=hr){
			goto binread_err;
		}
		switch(ret.init){//ni nujno prav 07-06-05
			default:	
				memset((void *)ret.phist2,0,ret.npart*sizeof(*ret.phist2));
				//memset((void *)ret.phistl2,0,ret.npart*sizeof(*ret.phistl2));
				break;
			case 2:
			case 20:
			case 21:
			case 22:
				memset((void *)ret.phist2,0,ret.dim*ret.dim*sizeof(*ret.phist2));
				//memset((void *)ret.phistl2,0,ret.dim*ret.dim*sizeof(*ret.phistl2));
				break;
		}

	}

	
	fclose(fp);
	m=ret.npart;
	for(i=0;i<ret.dim;i++)//Initialization of empty sites
		for(j=0;j<ret.dim;j++){
			flg=1;
			for(k=0;k<ret.npart;k++){
				if((ret.p[k].i==i) && (ret.p[k].j==j)){
					flg=0;
					break;
				}
			}
			if(flg){
				ret.p[m].i=i;
				ret.p[m].j=j;
				m++;
			}
		}
	return ret;
binread_err:
	if(NULL!=fp) fclose(fp);
	if (0!=ret.dim)
		clean(&ret);
	ret.dim=0;
	logprintf("Error reading from'%s': %s\n",fname,serr);
	return ret;
}


/* Rutine for writing data in binary mode.Joa*/

int binwrite(char *fname,MODEL *pm){	//Call from main.c binwrite(fname,&mod), mod is MODEL type;
	FILE* fp;
	long hr;
	char serr[30]={""};

	fp=fopen(fname,"wb");//Open new file in binary mode for writring, 
	if(NULL==fp){
		sprintf(serr,"Can not open file!");
		goto binwrite_err;
	}
	logprintf("Writing binary file: '%s'\n",fname); //At Prompt:Writing binary file: '.\fill-0121.dat'
	hr=fwrite(MAGIC,sizeof(char),4,fp);
	if(4!=hr){
		goto binwrite_err;
	}
	hr=fwrite(pm,sizeof(MODEL),1,fp);//writes pm,with size size(MODEL), at fp
	if(1!=hr){
		goto binwrite_err;
	}
	hr=fwrite(pm->p,sizeof(PARTICLE),pm->npart,fp);
	if(pm->npart!=hr){
		goto binwrite_err;
	}
	hr=fwrite(&pm->pE[0],sizeof(double),pm->nstartMC+pm->nMC+1,fp);
	if((pm->nstartMC+pm->nMC+1)!=hr){
		goto binwrite_err;
	}
	hr=bwrite_dmat(pm->pc11,-pm->dim,pm->dim,-pm->dim,pm->dim,fp);
	if(0!=hr){
		goto binwrite_err;
	}
	hr=bwrite_dmat(pm->pc22,-pm->dim,pm->dim,-pm->dim,pm->dim,fp);
	if(0!=hr){
		goto binwrite_err;
	}
	hr=bwrite_dmat(pm->pc12,-pm->dim,pm->dim,-pm->dim,pm->dim,fp);
	if(0!=hr){
		goto binwrite_err;
	}
	hr=bwrite_dmat(pm->pc21,-pm->dim,pm->dim,-pm->dim,pm->dim,fp);
	if(0!=hr){
		goto binwrite_err;
	}
	switch(pm->init){
		default:	
			hr=fwrite(&pm->phist[0],sizeof(pm->phist[0]),pm->npart,fp);
			//hr=fwrite(&pm->phistl[0],sizeof(pm->phistl[0]),pm->npart,fp);
			if((pm->npart)!=hr){
				goto binwrite_err;
			}
			hr=fwrite(&pm->phist2[0],sizeof(pm->phist2[0]),pm->npart,fp);
			//hr=fwrite(&pm->phistl2[0],sizeof(pm->phistl2[0]),pm->npart,fp);
			if((pm->npart)!=hr){
				goto binwrite_err;
			}
			break;
		case 2:
		case 20:
		case 21:
		case 22:
			hr=fwrite(&pm->phist[0],sizeof(pm->phist[0]),pm->nstartMC+pm->nMC+1,fp);
			//hr=fwrite(&pm->phistl[0],sizeof(pm->phistl[0]),pm->nstartMC+pm->nMC+1,fp);
			if((pm->nstartMC+pm->nMC+1)!=hr){
				goto binwrite_err;
			}
			break;
	}
	fclose(fp);
	return 0;
binwrite_err:
	if(NULL!=fp) fclose(fp);
	logprintf("Error writing to '%s': %s\n",fname,serr);
	return 1;


} 
/*?????????*/
void logprintf(char* format, ...){
	va_list marker;

	va_start(marker,format);
	vfprintf(stderr,format,marker);
	if(NULL!=fplog) vfprintf(fplog,format,marker);
}


void writeBA(MODEL *pm, LUPBA *pba,char *fname){
	FILE *outfp;
	long i;
	double dx;
	long norm;


	switch(pm->init){
		default:
			norm=pm->npart;
			break;
		case 2:
		case 20:
		case 21:
		case 22:
			norm=pm->dim*pm->dim;
			break;
	}

	outfp=fopen(fname,"w");
	if(NULL==outfp){
		logprintf("Can not open: %s\n",fname);
		exit(1);
	}
	fprintf(outfp,"%ld\n",pba->dim);
	fprintf(outfp,"%lg\n",pba->minx/norm);
	fprintf(outfp,"%lg\n",pba->maxx/norm);
	dx=(pba->maxx-pba->minx)/norm/(pba->dim-1);
	for(i=0;i<pba->dim;i++) fprintf(outfp,"%lg\t%lg\t%lg\n",pba->pb[i],pba->pg[i],pba->minx/norm+i*dx);
	fclose(outfp);
}

long readBA(MODEL *pm, LUPBA *pba,char *fname){
	FILE *outfp; 
	long i;

	long norm;


	switch(pm->init){
		default:
			norm=pm->npart;
			break;
		case 2:
		case 20:
		case 21:
		case 22:
			norm=pm->dim*pm->dim;
			break;
	}
	outfp=fopen(fname,"r");
	if(NULL==outfp){
		logprintf("Can not open: %s\n",fname);
		return(1);
	}
	logprintf("Reading beta from: %s\n",fname);
	fscanf(outfp,"%ld\n",&pba->dim);
	fscanf(outfp,"%lf\n",&pba->minx);
	fscanf(outfp,"%lf\n",&pba->maxx);
	pba->minx*=norm;
	pba->maxx*=norm;
	pba->pa=0;
	pba->pb=0;
	pba->pg=0;
	VBInitBA(pba);
	for(i=0;i<pba->dim;i++) fscanf(outfp,"%lf\t%lf\n%*s\n",pba->pb+i,pba->pg+i);
	fclose(outfp);
	VBPrepBA(pba);
	return(0);
}


void writeh(MODEL *pm, LUPBA *pba,char *fname){//??Joa
	FILE *outfp;
	long i;
	LUPFUNC hist;//??Joa
	double dx;
	long nsit;


	switch(pm->init){
		default:
			hist=histogram(pm->pE+pm->nstartMC+1,pm->nMC,pba->minx/pm->npart,pba->maxx/pm->npart,pba->dim);
			break;
		case 2:
		case 20:
		case 21:
		case 22:
			nsit=pm->dim*pm->dim;
			hist=histogram(pm->pE+pm->nstartMC+1,pm->nMC,pba->minx/nsit,pba->maxx/nsit,pba->dim);
			break;
	}
	outfp=fopen(fname,"w");
	if(NULL==outfp){
		logprintf("Can not open: %s\n",fname);
		exit(1);
	}
	dx=(hist.maxx-hist.minx)/(hist.dim-1);
	for(i=0;i<hist.dim;i++){
		fprintf(outfp,"%lg\t%lg\n",hist.minx+i*dx,hist.py[i]/pm->nMC);
	}
	fclose(outfp);
	free_dvector(hist.py,0,hist.dim-1);
}


void write_rho_h(MODEL *pm, char *fname){
	FILE *outfp;
	long i;
	LUPFUNC hist;
	double dx;
	long nsit;


	nsit=pm->dim*pm->dim; 
	hist=lhistogram(pm->phist+pm->nstartMC+1,pm->nMC,0,nsit,nsit+1);
	outfp=fopen(fname,"w");
	if(NULL==outfp){
		logprintf("Can not open: %s\n",fname);
		exit(1);
	}
	dx=(hist.maxx-hist.minx)/(hist.dim-1);
	for(i=0;i<hist.dim;i++){
		fprintf(outfp,"%lg\t%lg\n",i/(double)nsit,hist.py[i]/pm->nMC);
	}
	fclose(outfp);
	free_dvector(hist.py,0,hist.dim-1);
}
