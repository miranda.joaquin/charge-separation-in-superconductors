#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <string.h>
#include "nrutil.h"
#include "cluster.h"
//#include <cmath>



//unsigned long set_lkt[sizeof(unsigned long)];
//unsigned long reset_lkt[sizeof(unsigned long)];
//int ull=sizeof(unsigned long);

_inline double vcol(int i, int j){
	return 1./sqrt(i*i+j*j);
}

_inline double vjell(int i, int j,int N){

	return -(((-((0.5 + j)*log(-0.5 - i + sqrt(pow(0.5 + i,2) + pow(0.5 + j,2)))) - 
         (0.5 + i)*log(-0.5 - j + sqrt(pow(0.5 + i,2) + pow(0.5 + j,2))) + 
         (0.5 + i)*log(-0.5 - j + sqrt(pow(0.5 + j,2) + pow(0.5 + i - N,2))) - 
         N*log(-0.5 - j + sqrt(pow(0.5 + j,2) + pow(0.5 + i - N,2))) + 
         (0.5 + j)*log(-0.5 - i + sqrt(pow(0.5 + i,2) + pow(0.5 + j - N,2))) - 
         N*log(-0.5 - i + sqrt(pow(0.5 + i,2) + pow(0.5 + j - N,2))) + 
         (0.5 + j)*log(-0.5 - i + sqrt(pow(0.5 + j,2) + pow(0.5 + i - N,2)) + N) + 
         (0.5 + i)*log(-0.5 - j + sqrt(pow(0.5 + i,2) + pow(0.5 + j - N,2)) + N) - 
         (0.5 + j)*log(-0.5 - i + sqrt(pow(0.5 + i - N,2) + pow(0.5 + j - N,2)) + N) + 
         N*log(-0.5 - i + sqrt(pow(0.5 + i - N,2) + pow(0.5 + j - N,2)) + N) - 
         (0.5 + i)*log(-0.5 - j + sqrt(pow(0.5 + i - N,2) + pow(0.5 + j - N,2)) + N) + 
         N*log(-0.5 - j + sqrt(pow(0.5 + i - N,2) + pow(0.5 + j - N,2)) + N)))/pow(N,2));
}

_inline double vint(int i, int j,MODEL *pm){
	double r2;
	double i2,j2;
	double norm;

	r2=i*i+j*j;
	i2=i*i;
	j2=j*j;
	norm=pm->range*pm->range*pm->range*pm->range*exp(-4.);		//norm=range*range*exp(-4).Joa
//	return -pm->g2*(r2-pm->anizo*labs(i*j))*exp(-sqrt(r2)/pm->range);
//	return -pm->g2*((i2-j2)*(i2-j2)/4-pm->anizo*i2*j2)*exp(-sqrt(r2)/pm->range)/r2/r2;
	return -pm->g2*((i2-j2)*(i2-j2)/4-pm->anizo*i2*j2)*exp(-4*sqrt(r2)/pm->range)/norm;//g2*(i2-j2)*(i2-j2)- anizo*i2*j2*exp(-4sqrt(r2)/range)/norm

}

_inline double vint1(int i, int j,MODEL *pm){
	double r2;

	r2=i*i+j*j;
	if(1==r2) return pm->g2*vcol(1,0);
	if(2==r2) return pm->range*vcol(1,0);
	if(4==r2) return pm->anizo*vcol(1,0);
	return 0;

}

_inline double vint_per(int i, int j,MODEL *pm){
	double r2;

	i=labs(i);
	j=labs(j);
	i=__min(i,pm->dim-i);
	j=__min(j,pm->dim-j);
	r2=i*i+j*j;
	if(1==r2) return pm->g2*vcol(1,0);
	if(2==r2) return pm->range*vcol(1,0);
	if(4==r2) return pm->anizo*vcol(1,0);
	return 0;

}


_inline double f(int i, int j){
	return 1./sqrt(i*i+j*j);
}


_inline double S(int m,int n,int dim,long nmax){
	int i,j;
	double sum=0;

	nmax*=dim;
	for(i=dim;i<nmax;i+=dim){
		for(j=dim;j<nmax;j+=dim){
			sum+=f(m+i,n+j) + f(m-i,n-j)+f(m+i,n-j)+f(m-i,n+j)-4*f(i,j);
		}
	}
	for(i=dim;i<nmax;i+=dim){
		sum+=f(m+i,n)+f(m-i,n)+f(m,n+i)+f(m,n-i)-4*f(i,0);
	}
	sum+=f(m,n);
	return sum;
}

_inline double ift(long m, long n, long dim, double ** ppvck){
	long i,j;
	double ret,ph;

	ret=0;
	ph=2.*PI/dim;
	for(i=0;i<dim;i++)
		for(j=0;j<dim;j++){
			ret+=cos(ph*(i*m+j*n))*ppvck[i][j];
		}
	ret/=dim;
	return ret;
}

_inline void calc_vcol_per(MODEL *pm){//Rutine for assing vck =S and ppvc=ift. Comes from initpotencial
	double **vck;
	long i,j;

	vck=dmatrix(0,pm->dim,0,pm->dim);	
	for(i=0;i<pm->dim;i++){
		for(j=i+1;j<pm->dim;j++){
			vck[i][j]=S(i,j,pm->dim,100);//S is contribution of the form 1/(i*i+j*j). Why up to 100?. Joa
			vck[j][i]=vck[i][j];
		}
	}
	for(i=1;i<pm->dim;i++){
		vck[i][i]=S(i,i,pm->dim,100);
	}
	vck[0][0]=0;
	for(i=0;i<pm->dim;i++){// Non diagonal terms?. Joa
		for(j=i+1;j<pm->dim;j++){				//ppvc auxilary array for the precalculated cloumb interaction
			pm->ppvc[i][j]=ift(i,j,pm->dim,vck);//ift of S contribution trought vck, S is a sum of terms like 1/(i*i+j*j). Joa
			pm->ppvc[-i][-j]=pm->ppvc[i][j];
			pm->ppvc[i][-j]=pm->ppvc[i][j];
			pm->ppvc[-i][j]=pm->ppvc[i][j];
			pm->ppvc[j][i]=pm->ppvc[i][j];
			pm->ppvc[-j][-i]=pm->ppvc[i][j];
			pm->ppvc[j][-i]=pm->ppvc[i][j];
			pm->ppvc[-j][i]=pm->ppvc[i][j];
		}
	}
	for(i=1;i<pm->dim;i++){//Diagonal terms
			pm->ppvc[i][i]=ift(i,i,pm->dim,vck);
			pm->ppvc[-i][-i]=pm->ppvc[i][i];
			pm->ppvc[i][-i]=pm->ppvc[i][i];
			pm->ppvc[-i][i]=pm->ppvc[i][i];
	}
	pm->ppvc[0][0]=0;
	free_dmatrix(vck,0,pm->dim,0,pm->dim);
}



void initpot(MODEL *pm){//Goes to rutine for getting parameters g2, anizo, range.Joa
	int kx,ky;
	double Ejell;
	double (*pvint)(int,int,MODEL *);

	switch(pm->pottype){
		case 0:
		default:
			pvint=vint;
			break;
		case 1:
		case 2:
			pvint=vint1;
			break;
		case 3:
		case 4:
			pvint=vint_per;
			break;
	}

	switch(pm->pottype){
		case 0:
		case 1:
		default:	//Coulomb+pvint+vjell+Ejell. Joa
			for(kx=-pm->dim;kx<=pm->dim;kx++)
				for(ky=-pm->dim;ky<=pm->dim;ky++){
					if(labs(kx)+labs(ky)!=0){
						pm->ppvc[kx][ky]=vcol(kx,ky);	/// takes coulomb interaction. Joa
						pm->ppvi[kx][ky]=pvint(kx,ky,pm);///takes parameter values. g2,anizo, range. Joa
					}
				}
			Ejell=(2.*(1-sqrt(2))+3.*log((sqrt(2.)+1.)/(sqrt(2.)-1.)))/(3.*pm->dim);//Potential energy of jellium
			for(kx=0;kx<pm->dim;kx++)
				for(ky=0;ky<pm->dim;ky++){
						pm->ppvjell[kx][ky]=vjell(kx,ky,pm->dim)+Ejell;
				}
			break;
		case 2:
		case 3:	// only pvint. Joa
			for(kx=-pm->dim;kx<=pm->dim;kx++)
				for(ky=-pm->dim;ky<=pm->dim;ky++){
					if(labs(kx)+labs(ky)!=0){
						pm->ppvc[kx][ky]=0;//vcol(kx,ky);
						pm->ppvi[kx][ky]=pvint(kx,ky,pm);
					}
				}
//			Ejell=1./3./pm->dim*(2.*(1-sqrt(2))+3.*log((sqrt(2.)+1.)/(sqrt(2.)-1.)));//Potential energy of jellium
			for(kx=0;kx<pm->dim;kx++)
				for(ky=0;ky<pm->dim;ky++){
						pm->ppvjell[kx][ky]=0;//vjell(kx,ky,pm->dim)+Ejell;
				}
			break;
		case 4: //Coulomb + interaction; periodic boundary conditions
			for(kx=-pm->dim;kx<=pm->dim;kx++)
				for(ky=-pm->dim;ky<=pm->dim;ky++){
					if(labs(kx)+labs(ky)!=0){
						pm->ppvi[kx][ky]=pvint(kx,ky,pm);
					}
				}
			for(kx=0;kx<pm->dim;kx++)
				for(ky=0;ky<pm->dim;ky++){
						pm->ppvjell[kx][ky]=0;//vjell(kx,ky,pm->dim)+Ejell;
				}
			calc_vcol_per(pm);
/*			for(kx=-pm->dim+1;kx<pm->dim;kx++){
				for(ky=-pm->dim+1;ky<pm->dim;ky++){
					logprintf("%lg\t",pm->ppvc[kx][ky]);
				}
				logprintf("\n");
			}
			exit(0);
*/			break;
	}

}

int initialize(MODEL *pm){	//It's called by initialize(&mod). mod and pm are same kind of pointers class. Joa
//	int i;
//	unsigned long ul;

	pm->p=(PARTICLE*)malloc(pm->dim*pm->dim*sizeof(PARTICLE));
	pm->npart=pm->npart<=pm->dim*pm->dim?pm->npart:pm->dim*pm->dim;
	pm->pE=dvector(0,pm->nstartMC+pm->nMC);						//assigned space to energy's array
	pm->ppvc=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->ppvi=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->ppvjell=dmatrix(0,pm->dim-1,0,pm->dim-1);
	//pm->pc=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);//Joa
	pm->pc11=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->pc22=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->pc12=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->pc21=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);
	pm->ftdim=FFTNextPowerOfTwo(pm->dim)/2;
	pm->ppft=dmatrix(-pm->ftdim,pm->ftdim,0,pm->ftdim);
//	pm->ppaux=lmatrix(0,pm->dim-1,0,pm->dim-1);	//long **lmatrix(long nrl, long nrh, long ncl, long nch).Space assignation to ppaux.Joa
	pm->ppaux=lmatrix(-pm->dim,pm->dim+pm->dim-1,-pm->dim,pm->dim+pm->dim-1);
	pm->conduct=dvector(0,10*pm->nMC);	
	switch(pm->init){	//What is it doing?. init is option for initialize cluster counting?
		default:	
			pm->phist=lvector(1,pm->npart);	//phist=lvector=paux1?? Joa
			//pm->phistl=lvector(1,pm->npart);//
			pm->phist2=dvector(1,pm->npart);
			//pm->phistl2=dvector(1,pm->npart);//
			pm->paux1=lvector(1,pm->npart);
			//pm->pauxl=lvector(1,pm->npart);//
			cleanhist(pm);
			break;
		case 2:
		case 20:
		case 21:
		case 22:
//			pm->phist=lvector(1,pm->dim*pm->dim);
			pm->phist=lvector(0,pm->nstartMC+pm->nMC);
			//pm->phistl=lvector(0,pm->nstartMC+pm->nMC);
			pm->phist2=0;
			//pm->phistl2=0;
//			pm->phist2=dvector(1,pm->dim*pm->dim);
			pm->paux1=0;//Initialize counter for counting hole clusters?Joa
			//pm->pauxl=0;
			break;
	}
	//pm->ppauxl=lmatrix(0,pm->dim-1,0,pm->dim-1);
	initpot(pm);
	switch(pm->init){
		case 0:
		default:
			seed_random(pm);
			break;
		case 1:
			seed_center(pm);
			break;
		case 21:
			seed_center_uniform(pm);
			break;

	}


//	for(i=0,ul=1;i<ull;i++,ul<<1){
//		set_lkt[i]=ul;
//		reset_lkt[i]=~ul;
//	}
//	pm->px1=cmatrix(0,pm->dim-1,0,pm->dim-1);
//	pm->py1=cmatrix(0,pm->dim-1,0,pm->dim-1);
//	pm->px=lvector(0,pm->dim*pm->dim/sizeof(unsigned long)+1);
//	pm->py=lvector(0,pm->dim*pm->dim/sizeof(unsigned long)+1);
	return 0;
}

void clean(MODEL *pm){
//	free_cmatrix(pm->px,0,pm->dim-1,0,pm->dim-1);
//	free_cmatrix(pm->py,0,pm->dim-1,0,pm->dim-1);
	free(pm->p);

	free_dmatrix(pm->ppvi,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->ppvc,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->ppvjell,0,pm->dim-1,0,pm->dim-1);
	//free_dmatrix(pm->pc,-pm->dim,pm->dim,-pm->dim,pm->dim);//Joa
	free_dmatrix(pm->pc11,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->pc22,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->pc12,-pm->dim,pm->dim,-pm->dim,pm->dim);
	free_dmatrix(pm->pc21,-pm->dim,pm->dim,-pm->dim,pm->dim);
//	free_lmatrix(pm->ppaux,0,pm->dim-1,0,pm->dim-1);
	free_lmatrix(pm->ppaux,-pm->dim,pm->dim+pm->dim-1,-pm->dim,pm->dim+pm->dim-1);
//	free_dvector(pm->conduct,0,pm->nMC);
	switch(pm->init){
		default:	
			free_lvector(pm->phist,1,pm->npart);
			//free_lvector(pm->phistl,1,pm->npart);
			free_dvector(pm->phist2,1,pm->npart);
			//free_dvector(pm->phistl2,1,pm->npart);
			free_lvector(pm->paux1,1,pm->npart);
			//free_lvector(pm->pauxl,1,pm->npart);
			break;
		case 2:
		case 20:
		case 21:
		case 22:
//			free_lvector(pm->phist,1,pm->dim*pm->dim);
			free_lvector(pm->phist,0,pm->nstartMC+pm->nMC);
			//free_lvector(pm->phistl,0,pm->nstartMC+pm->nMC);
//			free_dvector(pm->phist2,1,pm->dim*pm->dim);
//			free_lvector(pm->paux1,1,pm->dim*pm->dim);
			break;
	}
	//free_lmatrix(pm->ppauxl,0,pm->dim-1,0,pm->dim-1);
	if(NULL!=pm->ppft) free_dmatrix(pm->ppft,-pm->ftdim,pm->ftdim,0,pm->ftdim);
	if(NULL!=pm->pE) free_dvector(pm->pE,0,pm->nstartMC+pm->nMC);
	if(NULL!=pm->conduct) free_dvector(pm->conduct,0,10*pm->nMC);
	pm->p=0;
	pm->ppvi=0;
	pm->ppvc=0;
	pm->pE=0;
	//pm->pc=0;//Joa
	pm->pc11=0;
	pm->pc22=0;
	pm->pc12=0;
	pm->pc21=0;
	pm->ppft=0;
}

_inline swappart(int i, int j,MODEL *pm){
	PARTICLE tmp;

	tmp=pm->p[i];
	pm->p[i]=pm->p[j];
	pm->p[j]=tmp;

}



void seed_random(MODEL *pm){	//Call from main.c (seed_random(&mod))
	long i,j,dim;

	dim=0;
	for(i=0;i<pm->dim;i++)
		for(j=0;j<pm->dim;j++){
			pm->p[dim].i=i;
			pm->p[dim].j=j;
			dim++;
		}

	for(i=0,dim=pm->dim*pm->dim;i<pm->npart;i++,dim--){

		j=i+(long)(dim*drand());
		swappart(i,j,pm);
		pm->p[i].type=(char)(drand()+.5);
	}
}


void seed_center(MODEL *pm){
	long i,j,dim,n,k;

	dim=0;
	for(i=0;i<pm->dim;i++)
		for(j=0;j<pm->dim;j++){
			pm->p[dim].i=i;
			pm->p[dim].j=j;
			dim++;
		}

	dim=(long)sqrt((double)pm->npart)/2+1;
//	fprintf(stderr,"%ld\n",dim);
	k=0;
	for(i=pm->dim/2-dim;i<pm->dim/2+dim && k<pm->npart;i++)
		for(j=pm->dim/2-dim;j<pm->dim/2+dim && k<pm->npart;j++){
			n=i*pm->dim+j;
			swappart(k,n,pm);
			pm->p[k].type=(char)(drand()+.5);
			k++;
		}
}

void seed_center_uniform(MODEL *pm){
	long i,j,dim,n,k;

	dim=0;
	for(i=0;i<pm->dim;i++)
		for(j=0;j<pm->dim;j++){
			pm->p[dim].i=i;
			pm->p[dim].j=j;
			dim++;
		}

	dim=(long)sqrt((double)pm->npart)/2+1;
//	fprintf(stderr,"%ld\n",dim);
	k=0;
	for(i=pm->dim/2-dim;i<pm->dim/2+dim && k<pm->npart;i++)
		for(j=pm->dim/2-dim;j<pm->dim/2+dim && k<pm->npart;j++){
			n=i*pm->dim+j;
			swappart(k,n,pm);
			pm->p[k].type=0;
			k++;
		}
}


_inline double pot(int i,int j,MODEL *pm){// Sum of coulomb potential + vint(g2 +anizo +range)
	double ret;
	long kx,ky;
//	double zero=0;

	kx=pm->p[i].i-pm->p[j].i;
	ky=pm->p[i].j-pm->p[j].j;
//	ret=vcol(kx,ky);
	ret=pm->ppvc[kx][ky];// ppvc = vcol(kx,ky).Joa
//	ret+=pm->g2*(pm->p[i].type==pm->p[j].type?1.:-1.)*vint(kx,ky,pm);
	ret+=(pm->p[i].type==pm->p[j].type?1.:-1.)*pm->ppvi[kx][ky];	//ppvi is in intpot. Takes parameter values. g2,anizo, range. Joa
//	if(fabs(ret)>100){
//        zero=0;
//    }

	return ret;
}

_inline double singeint(int i,MODEL *pm){
	long dim,n;
	double ret;
	
	n=i<pm->npart?i:pm->npart;
	ret=0;
	for(dim=0;dim<n;dim++){
		ret+=pot(i,dim,pm);
	}

	for(dim=n+1;dim<pm->npart;dim++){
		ret+=pot(i,dim,pm);
	}
	ret+=pm->ppvjell[pm->p[i].i][pm->p[i].j]*pm->npart;
	return ret;
}

_inline double nobipolaronint(int i,int k,MODEL *pm){
	long dim,n;
	double St=0.0;
	double ret;
	
	n=i<pm->npart?i:pm->npart;
	ret=0;
//	ret+=(pm->p[i].type==0?1.:-1.)*St;//Including uniaxial stress
	for(dim=0;dim<n;dim++){
		if(k==dim){ 
			ret+=0;}
		else{ 
			ret+=pot(i,dim,pm);}
	}
	for(dim=n+1;dim<pm->npart;dim++){
		if(k==dim){ 
			ret+=0;}
		else{ 
			ret+=pot(i,dim,pm);}
	}
	ret+=pm->ppvjell[pm->p[i].i][pm->p[i].j]*pm->npart; 
	return ret;
}

double tot_eng(MODEL *pm){	//From mod.pE[iMC]=tot_eng(&mod)/mod.npart, pE[iMC] energy of two next steps. Joa
	long dim,n;
	double ret;
	ret=0;	//Sum of ppvjell + pot [Sum of coulomb potential + vint(= g2 +anizo +range)]. Joa
	for(dim=0;dim<pm->npart;dim++){
		ret+=pm->ppvjell[pm->p[dim].i][pm->p[dim].j]*pm->npart;
		for(n=dim+1;n<pm->npart;n++){
			ret+=pot(dim,n,pm);
		}
	}
	return ret;
}


double ordpar(MODEL *pm){
	long i;
	long j;
	for(i=0,j=0;i<pm->npart;i++){
		j+=pm->p[i].type;
	}
	return (j-0.5*pm->npart)*2/(pm->dim*pm->dim);
}


void MCStep(MODEL *pm){ //one MC step
	long i;
	long dim,n;
	double e;
//	double etmp;
//	double foo;

	n=pm->dim*pm->dim-pm->npart;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){

 		dim=pm->npart+(long)(n*drand());
		pm->p[dim].type=(char)(drand()+.5);
//		e=tot_eng(pm);
		e=singeint(i,pm);
		swappart(i,dim,pm);
//		e-=tot_eng(pm);
		e-=singeint(i,pm);

//		if(fabs(etmp/e-1)>1e-6)
//			fprintf(stderr,"%lg ",etmp/e-1);
		if(!(e>=0 || drand()<=exp(e/pm->T))){
			swappart(i,dim,pm);
			pm->acpt-=1;
		}
	}
	pm->acpt/=pm->npart;
}




_inline long find_neighb_holes(long i,long *nlist,MODEL *pm){
	long n,in;
	n=0;

	if(pm->p[i].i>0){
		in=pm->p[i].i-1;
		if(pm->p[i].j>0) 
			if(pm->ppaux[in][pm->p[i].j-1]>=pm->npart){
				nlist[n]=pm->ppaux[in][pm->p[i].j-1];
				n++;
			}
		if(pm->ppaux[in][pm->p[i].j]>=pm->npart){
			nlist[n]=pm->ppaux[in][pm->p[i].j];
			n++;
		}
		if(pm->p[i].j<pm->dim-1) 
			if(pm->ppaux[in][pm->p[i].j+1]>=pm->npart){
				nlist[n]=pm->ppaux[in][pm->p[i].j+1];
				n++;
				}
	}
	if(pm->p[i].i<pm->dim-1){
		in=pm->p[i].i+1;
		if(pm->p[i].j>0) 
			if(pm->ppaux[in][pm->p[i].j-1]>=pm->npart){
				nlist[n]=pm->ppaux[in][pm->p[i].j-1];
				n++;
			}
		if(pm->ppaux[in][pm->p[i].j]>=pm->npart){
			nlist[n]=pm->ppaux[in][pm->p[i].j];
			n++;
		}
		if(pm->p[i].j<pm->dim-1) 
			if(pm->ppaux[in][pm->p[i].j+1]>=pm->npart){
				nlist[n]=pm->ppaux[in][pm->p[i].j+1];
				n++;
				}
	}
	in=pm->p[i].i;
	if(pm->p[i].j>0) 
		if(pm->ppaux[in][pm->p[i].j-1]>=pm->npart){
			nlist[n]=pm->ppaux[in][pm->p[i].j-1];
			n++;
		}
	if(pm->p[i].j<pm->dim-1) 
		if(pm->ppaux[in][pm->p[i].j+1]>=pm->npart){
			nlist[n]=pm->ppaux[in][pm->p[i].j+1];
			n++;
			}
	return n;
}


void MCStep_Neigh(MODEL *pm){ //one MC step with different dynamics
	long i,j;
	long dim,n;
	double e;
	long nlist[8];
//	double etmp;
//	double foo;



	j=pm->dim*pm->dim;
	for(i=0;i<pm->npart;i++)
		pm->ppaux[pm->p[i].i][pm->p[i].j]=i;
	for(i=pm->npart;i<j;i++)
		pm->ppaux[pm->p[i].i][pm->p[i].j]=i;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){
		n=find_neighb_holes(i,nlist,pm);//find neighbor holes
		if(n>0){
 			dim=nlist[(long)(n*drand())];
			pm->p[dim].type=(char)(drand()+.5);
			e=singeint(i,pm);
			swappart(i,dim,pm);
			e-=singeint(i,pm);
			if(!(e>=0 || drand()<=exp(e/pm->T))){
				swappart(i,dim,pm);
				pm->acpt-=1;
			}
		}
		else pm->acpt-=1;

	}
	pm->acpt/=pm->npart;
}



_inline double lupba(double e,LUPBA *pba,MODEL *pm){// returns log(weight(e))

	double dx;
	long i;

//	return(-e/pm->T);
	if(e<=pba->minx) return -pba->pb[0]*e+pba->pa[0];//-(e-pba->minx)/pm->T;
	if(e>=pba->maxx) return -pba->pb[pba->dim-1]*e+pba->pa[pba->dim-1];// -(e-pba->maxx)/pm->T;
//	if(e>pba->maxx) return -pba->pb[pba->dim-1]*pba->maxx+pba->pa[pba->dim-1];
	dx=(pba->maxx-pba->minx)/(pba->dim-1);
	i=(long) ((e-pba->minx)/dx);
	return (-pba->pb[i]*e+pba->pa[i]);

	
}

_inline double eintrem(int i,MODEL *pm){
	long dim;
	double ret;

	ret=0;
	for(dim=0;dim<i;dim++){
		ret+=pot(i,dim,pm);
//		ret+=pm->ppvjell[pm->p[dim].i][pm->p[dim].j];
	}

	for(dim=i+1;dim<pm->npart;dim++){
		ret+=pot(i,dim,pm);
//		ret+=pm->ppvjell[pm->p[dim].i][pm->p[dim].j];
	}
//	ret+=pm->ppvjell[pm->p[i].i][pm->p[i].j]*pm->npart-pm->mu;
	ret+=-pm->mu;
	return ret;
}

_inline double eintadd(int i,MODEL *pm){
	long dim;
	double ret;
	
	ret=0;
	for(dim=0;dim<pm->npart;dim++){
		ret+=pot(i,dim,pm);
//		ret+=pm->ppvjell[pm->p[dim].i][pm->p[dim].j];
	}
//	ret+=pm->ppvjell[pm->p[i].i][pm->p[i].j]*(pm->npart+1)-pm->mu;
	ret+=-pm->mu;
	return ret;
}




void MCStep_multi(MODEL *pm,LUPBA *pba){ //one multicanonical MC step  //typedef struct tagLUPBA in Cluster.h
	long i;
	long dim,n;
	double e,te;
//	double etmp;
	double foo;
	double eavg=0;
	long ecount=0;

	te=tot_eng(pm);
	n=pm->dim*pm->dim-pm->npart;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){
 		dim=pm->npart+(long)(n*drand());
		pm->p[dim].type=(char)(drand()+.5);
//		e=tot_eng(pm);
		e=singeint(i,pm);
		swappart(i,dim,pm);
//		e-=tot_eng(pm);
		e-=singeint(i,pm);

//		if(fabs(etmp/e-1)>1e-6)
//			fprintf(stderr,"%lg ",etmp/e-1);
//		if(!(e>=0 || drand()<=exp(e/pm->T))){
		foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
		if(foo<0 && drand()>exp(foo)){
			swappart(i,dim,pm);
			pm->acpt-=1;
		}
		else te-=e;
	}
	if(0!=e){
		eavg+=fabs(e);
		ecount++;
	}

	pm->pE[0]+=eavg/ecount;
	pm->acpt/=pm->npart;
}

void MCStepMu_rho(MODEL *pm,LUPBA *pw){ //one MC step with fixed chemical potential with density weight
	long i,j;
	long n,dim;
//	double r,r0;
	double e,te;
	char type;
//	double tmp;
//	double foo;
//	double eavg;
//	long ecount;
	double b=1/pm->T;
 
	te=tot_eng(pm)-pm->npart*pm->mu;
	n=pm->dim*pm->dim;
	pm->acpt=n;
//	ecount=0;
//	eavg=0;
	for(i=0,e=0;i<n;i++){
 		dim=(long)(n*drand());
		j=(long)(3*drand());
//		tmp=tot_eng(pm)-pm->npart*pm->mu;
		switch(j){ 
			case 0: //we remove a particle if exists
				if(dim<pm->npart){
					e=eintrem(dim,pm);
					if(e>=0 || drand()<=exp(e*b+pw->pb[pm->npart-1])){
						swappart(dim,--pm->npart,pm);//move accepted
						te-=e;
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
					else
						pm->acpt--;
				}
				break;
			case 1: // we add or modify a particle
			case 2:
				if(dim<pm->npart){// we try to change one particle
					e=singeint(dim,pm);
					type=pm->p[dim].type;
					pm->p[dim].type=j/2;
					if(type!=pm->p[dim].type){
						e-=singeint(dim,pm);
						if(!(e>=0 || drand()<=exp(e*b))) {
							pm->p[dim].type=type;//move not accepted
							pm->acpt--;
						}
						else{
							te-=e;
//							tmp-=tot_eng(pm)-pm->npart*pm->mu;
//							fprintf(stderr,"%lg %lg\n",e,e-tmp);
						}
					}
				}
				else if(pm->npart<n) {//we try to add a new particle
					pm->p[dim].type=j/2;
					e=-eintadd(dim,pm);
					if(e>=0 || drand()<=exp(e*b-pw->pb[pm->npart])){
						swappart(dim,pm->npart++,pm);//move accepted
						te-=e;
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
				}
				else
					pm->acpt--;
				break;
		}
//		if(0!=e){
//			eavg+=fabs(e);
//			ecount++;
//		}
	}
	pm->acpt/=n;
//	pm->pE[0]+=eavg/ecount;
}




void MCStepMu_multi(MODEL *pm,LUPBA *pba){ //one MC step with fixed chemical potential
	long i,j;
	long n,dim;
//	double r,r0;
	double e,te;
	char type;
//	double tmp;
	double foo;
	double eavg;
	long ecount;
 
	te=tot_eng(pm)-pm->npart*pm->mu;
	n=pm->dim*pm->dim;
	pm->acpt=n;
	ecount=0;
	eavg=0;
	for(i=0,e=0;i<n;i++){
 		dim=(long)(n*drand());
		j=(long)(3*drand());
//		tmp=tot_eng(pm)-pm->npart*pm->mu;
		switch(j){ //we remove a particle
			case 0:
				if(dim<pm->npart){
					e=eintrem(dim,pm);
					foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
					if(foo>=0 || drand()<=exp(foo)){
						swappart(dim,--pm->npart,pm);//move accepted
						te-=e;
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
					else
						pm->acpt--;
				}
				break;
			case 1: // we add or modify a particle
			case 2:
				if(dim<pm->npart){// we try to swap one particle
					e=singeint(dim,pm);
					type=pm->p[dim].type;
					pm->p[dim].type=j/2;
					if(type!=pm->p[dim].type){
						e-=singeint(dim,pm);
						foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
						if(!(foo>=0 || drand()<=exp(foo))){
							pm->p[dim].type=type;//move not accepted
							pm->acpt--;
						}
						else{
							te-=e;
//							tmp-=tot_eng(pm)-pm->npart*pm->mu;
//							fprintf(stderr,"%lg %lg\n",e,e-tmp);
						}
					}
				}
				else if(pm->npart<n) {//we try to add a new particle
					pm->p[dim].type=j/2;
					e=-eintadd(dim,pm);
					foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
					if(foo>=0 || drand()<=exp(foo)){
						swappart(dim,pm->npart++,pm);//move accepted
						te-=e;
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
				}
				else
					pm->acpt--;
				break;
		}
		if(0!=e){
			eavg+=fabs(e);
			ecount++;
		}
	}
	pm->acpt/=n;
	pm->pE[0]+=eavg/ecount;
}



void MCStep_multi_new(MODEL *pm,LUPBA *pba){ //one multicanonical MC step
	long i,j,k;
	long dim,n;
	double e,te;
//	double etmp;
	double foo;
#define NSWAP 3
	long lhist[2][NSWAP];
	double dhist[2][NSWAP];

	te=tot_eng(pm);
	n=pm->dim*pm->dim-pm->npart;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){

//		te=tot_eng(pm);
		e=0;
		for(j=0;j<NSWAP;j++){
 			dim=pm->npart+(long)(n*drand());
			k=(long)(pm->npart*drand());
			lhist[0][j]=dim;
			lhist[1][j]=k;
			dhist[0][j]=pm->p[dim].type;
			dhist[1][j]=pm->p[k].type;
			pm->p[dim].type=(char)(drand()+.5);
			e+=singeint(k,pm);
			swappart(k,dim,pm);
			e-=singeint(k,pm);
		}
//		e=tot_eng(pm);
//		if(fabs(etmp/e-1)>1e-6)
//			fprintf(stderr,"%lg ",etmp/e-1);
//		if(!(e>=0 || drand()<=exp(e/pm->T))){
		foo=lupba(te-e,pba,pm)-lupba(te,pba,pm);
		if(foo<0 && drand()>exp(foo)){
			for(j=NSWAP-1;j>=0;j--){
				dim=lhist[0][j];
				k=lhist[1][j];
				swappart(k,dim,pm);
				pm->p[dim].type=dhist[0][j];
				pm->p[k].type=dhist[1][j];
			}
			pm->acpt-=1;
		}
		else te-=e;
	}
	pm->acpt/=pm->npart;
}





void MCStep_alt(MODEL *pm){ //one MC step
	long i;
	long dim,n;
	double e;
//	double etmp;
//	double foo;
//	PARTICLE tmp;

	n=pm->dim*pm->dim;//-pm->npart;
	pm->acpt=pm->npart;
	for(i=0;i<pm->npart;i++){
 		dim=(long)(n*drand());
		e=singeint(i,pm);
		if (dim<pm->npart){
			pm->p[i].type=pm->p[i].type==0?1:0;
		}
		else{
			swappart(i,dim,pm);
		}
		e-=singeint(i,pm);

		if(!(e>=0 || drand()<=exp(e/pm->T))){
			if (dim<pm->npart){
				pm->p[i].type=pm->p[i].type==0?1:0;
			}
			else{
				swappart(i,dim,pm);
			}
			pm->acpt-=1;
		}
	}
	pm->acpt/=pm->npart;
}




void MCStepMu(MODEL *pm){ //one MC step with fixed chemical potential
	long i,j;
	long n,dim;
//	double r,r0;
	double e;
	char type;
//	double tmp;
 
	n=pm->dim*pm->dim;
	pm->acpt=n;
	for(i=0;i<n;i++){
 		dim=(long)(n*drand());
		j=(long)(3*drand());
//		tmp=tot_eng(pm)-pm->npart*pm->mu;
		switch(j){
			case 0:
				if(dim<pm->npart){
					e=eintrem(dim,pm);
					if(e>=0 || drand()<=exp(e/pm->T)){
						swappart(dim,--pm->npart,pm);//move accepted
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
					else
						pm->acpt--;
				}
				else
					pm->acpt--;
				break;
			case 1:
			case 2:
				if(dim<pm->npart){// we just swap one particle, no need to care about jellium
					e=singeint(dim,pm);
					type=pm->p[dim].type;
					pm->p[dim].type=j/2;
					if(type!=pm->p[dim].type){
						e-=singeint(dim,pm);
						if(!(e>=0 || drand()<=exp(e/pm->T))) {
							pm->p[dim].type=type;//move not accepted
							pm->acpt--;
						}
//						else{
//							tmp-=tot_eng(pm)-pm->npart*pm->mu;
//							fprintf(stderr,"%lg %lg\n",e,e-tmp);
//						}
					}
					else
						pm->acpt--;

				}
				else if(pm->npart<n) {//we add a new particle
					pm->p[dim].type=j/2;
					e=-eintadd(dim,pm);
					if(e>=0 || drand()<=exp(e/pm->T)){
						swappart(dim,pm->npart++,pm);//move accepted
//						tmp-=tot_eng(pm)-pm->npart*pm->mu;
//						fprintf(stderr,"%lg %lg\n",e,e-tmp);
					}
				}
				else
					pm->acpt--;
				break;
		}
	}
	pm->acpt/=n;
}



_inline  int is_in(int kmin,int kmax,int i){
	return (kmin<i)&&(kmax>i);
}



void rrcor(int mode,double part, MODEL *pm){ //density-density correlation
	long i,j;
	int min,max;

	if(1== mode){
		for(i=-pm->dim;i<=pm->dim;i++)
			for(j=-pm->dim;j<=pm->dim;j++)
				pm->pc11[i][j]=0;
		return;
	}

	if(1==part)
		for(i=0;i<pm->npart;i++)
			for(j=0;j<pm->npart;j++){
				pm->pc11[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;

			}
	else {
		min=(int)(pm->dim*(.5-part/2.));
		max=(int)(pm->dim*(.5+part/2.));
		for(i=0;i<pm->npart;i++)
			for(j=0;j<pm->npart;j++){
				if(is_in(min,max,pm->p[i].i)&&is_in(min,max,pm->p[i].j)&&is_in(min,max,pm->p[j].i)&&is_in(min,max,pm->p[j].j))
					pm->pc11[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;

			}
	}
}



void cor_per( MODEL *pm){ //modifies correlation matrix for the periodic case
	long i,j;
	double **pptmp;
	double **pp;
	
#define sign(x) ((x)<0?-1:((x)==0?0:1))

	pptmp=dmatrix(-pm->dim,pm->dim,-pm->dim,pm->dim);

	pp=pm->pc11;
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pptmp[i][j]=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pp[i][j]=pptmp[i][j];

	pp=pm->pc22;
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pptmp[i][j]=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pp[i][j]=pptmp[i][j];


	pp=pm->pc12;
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pptmp[i][j]=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pp[i][j]=pptmp[i][j];

	pp=pm->pc21;
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pptmp[i][j]=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	for(i=-pm->dim;i<=pm->dim;i++)
		for(j=-pm->dim;j<=pm->dim;j++)
			pp[i][j]=pptmp[i][j];

	free_dmatrix(pptmp,-pm->dim,pm->dim,-pm->dim,pm->dim);
#undef sign
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////



void gencor( MODEL *pm){ //calculates all four possible correlations. //(up-up,up-down, down-down, down-up).Joa
	long i,j;

	for(i=0;i<pm->npart;i++)
		if(0==pm->p[i].type)
			for(j=0;j<pm->npart;j++)
				if(0==pm->p[j].type)
					pm->pc11[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;
				else
					pm->pc12[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;
		else
			for(j=0;j<pm->npart;j++)
				if(0==pm->p[j].type)
					pm->pc21[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;
				else
					pm->pc22[pm->p[i].i-pm->p[j].i][pm->p[i].j-pm->p[j].j]++;
	pm->ncor++;
}

double getcorr_per(int i, int j,int type,MODEL *pm){//retrives specific correlation 
	double cor;
	double rhoavg=pm->npart/((double)pm->dim*pm->dim);
	double d11,d22,d12,d21;
	double **pp;
	if(pm->ncor==0) return 0;

#define sign(x) ((x)<0?-1:((x)==0?0:1))

	pp=pm->pc11;
	d11=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	pp=pm->pc22;
	d22=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	pp=pm->pc12;
	d12=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];
	pp=pm->pc21;
	d21=pp[i][j]+(i!=0?1:0)*pp[pm->dim*sign(i)-i][j]+(j!=0?1:0)*pp[i][pm->dim*sign(j)-j]+(j!=0?1:0)*(i!=0?1:0)*pp[pm->dim*sign(i)-i][pm->dim*sign(j)-j];


#undef sign

	switch(type){
	case 0:
	default:
		cor=d11;
		cor+=d22;
		cor+=d12;
		cor+=d21;
		cor-=pm->dim*pm->dim*rhoavg*rhoavg*pm->ncor;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0])*(1-rhoavg); // 13.11.05 I hope that this is correct now.
//		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]-(pm->dim)*(pm->dim)*rhoavg*rhoavg*pm->ncor);
//		cor/=pm->ncor;
		break;
	case 11:
		cor=d11;
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 22:
		cor=d22;
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 12:
		cor=d12;
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 21:
		cor=d21;
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 1: //spin-spin correlation
		cor=d11;
		cor+=d22;
		cor-=d12;
		cor-=d21;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	}
	return cor;
}



double getcorr_open(int ix, int iy,int type,MODEL *pm){//retrives specific correlation for open BC
	double cor;
	double rhoavg=pm->npart/((double)pm->dim*pm->dim);

	if(pm->ncor==0) return 0;
	switch(type){
	case 0:
	default:
		cor=pm->pc11[ix][iy];
		cor+=pm->pc22[ix][iy];
		cor+=pm->pc12[ix][iy];
		cor+=pm->pc21[ix][iy];
		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0])*(1-rhoavg); // 13.11.05 I hope that this is correct now.
//		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]-(pm->dim)*(pm->dim)*rhoavg*rhoavg*pm->ncor);
//		cor/=pm->ncor;
		break;
	case 11:
		cor=pm->pc11[ix][iy];
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 22:
		cor=pm->pc22[ix][iy];
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 12:
		cor=pm->pc12[ix][iy];
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 21:
		cor=pm->pc21[ix][iy];
//		cor-=(pm->dim-labs(ix))*(pm->dim-labs(iy))*rhoavg*rhoavg*pm->ncor/4;
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	case 1: //spin-spin correlation
		cor=pm->pc11[ix][iy];
		cor+=pm->pc22[ix][iy];
		cor-=pm->pc12[ix][iy];
		cor-=pm->pc21[ix][iy];
		cor/=((double)pm->pc11[0][0]+pm->pc22[0][0]);
		break;
	}
	return cor;
}

double getcorr(int ix, int iy,int type,MODEL *pm){//retrives specific correlation 
	switch(pm->pottype){
		default:
			return getcorr_open(ix,iy,type,pm);
			break;
		case 3:
		case 4:
			return getcorr_per(ix,iy,type,pm);
			break;
	}
}


void cleancor(MODEL *pm){ //clean all four correlation arrays
	long i,j;

		for(i=-pm->dim;i<=pm->dim;i++)
			for(j=-pm->dim;j<=pm->dim;j++)
			{
				pm->pc11[i][j]=0;
				pm->pc22[i][j]=0;
				pm->pc12[i][j]=0;
				pm->pc21[i][j]=0;
			}
		pm->ncor=0;
}


long FFTNextPowerOfTwo(long n)
{

		n=1<<(long)ceil(log((double)n)/log(2.));
		return n;
}


void four_trans(MODEL *pm){
	long i,j;
	double ***tdata,**tspeq;
	double tmp,tmp1;//,sum;
	long limit;

	limit=pm->ftdim*2;
//	return;
	tdata=f3tensor(1,1,1,limit,1,limit);
	tspeq=dmatrix(1,1,1,limit*2);
//	tmp=-(double)pm->npart/(pm->dim*pm->dim-pm->npart);
	tmp=0;
	for(i=1;i<=limit;i++)
		for(j=1;j<=limit;j++) 
		{
			if(j<=pm->dim && i<=pm->dim){
//				tdata[1][i][j]=1-cos(3*(i+j));
				tdata[1][i][j]=tmp;
			}
			else
				tdata[1][i][j]=0;
		}
	for(i=0;i<pm->npart;i++){
		tdata[1][pm->p[i].i+1][pm->p[i].j+1]=1;
	}
	for(i=1;i<=pm->dim;i++){//apodization
		tmp1=cos(PI * (i-pm->dim/2.)/pm->dim);
		tmp1*=tmp1;
		for(j=1;j<=pm->dim;j++)
		{
			tmp=cos(PI*(j-pm->dim/2.)/pm->dim);
			tmp*=tmp;
			tdata[1][i][j]*=tmp*tmp1; 
		}
	}
	rlft3(tdata,tspeq,1,limit,limit,1);
	for(i=1;i<=pm->ftdim;i++) //unscramble FFT
		for(j=1;j<=pm->ftdim;j++){
			tmp=tdata[1][i][2*j-1]*tdata[1][i][2*j-1];
			tmp+=tdata[1][i][2*j]*tdata[1][i][2*j];
			pm->ppft[i-1][j-1]=sqrt(tmp);
			tmp=tdata[1][limit-i+1][2*j-1]*tdata[1][limit-i+1][2*j-1];
			tmp+=tdata[1][limit-i+1][2*j]*tdata[1][limit-i+1][2*j]; 
			pm->ppft[-i][j-1]=sqrt(tmp);
		}

//	pm->ppft[0][0]=0;	
	free_dmatrix(tspeq,1,1,1,limit*2);
	free_f3tensor(tdata,1,1,1,limit,1,limit);
}




void four_trans_spin(MODEL *pm){
	long i,j,ir,jr;
	double ***tdata,**tspeq;
	double tmp,tmp1;//,sum;
	long limit;

	limit=pm->ftdim*2;
//	return;
	tdata=f3tensor(1,1,1,limit,1,limit);
	tspeq=dmatrix(1,1,1,limit*2);
//	tmp=-(double)pm->npart/(pm->dim*pm->dim-pm->npart);
	tmp=0;
	for(i=1;i<=limit;i++)
		for(j=1;j<=limit;j++) 
		{
			ir=i-pm->ftdim;
			jr=j-pm->ftdim;
			if(jr<=pm->dim && ir<=pm->dim && jr>=-pm->dim && ir>=-pm->dim){
//				tdata[1][i][j]=1-cos(3*(i+j));
				tdata[1][i][j]=getcorr(i-pm->ftdim,j-pm->ftdim,1,pm);
			}
			else
				tdata[1][i][j]=0;
		}
	for(i=1;i<=limit;i++){//apodization
		tmp1=cos(PI * (i-limit/2)/limit);
		tmp1*=tmp1;
		for(j=1;j<=limit;j++)
		{
			tmp=cos(PI*(j-limit/2.)/limit);
			tmp*=tmp;
			tdata[1][i][j]*=tmp*tmp1; 
		}
	}
	rlft3(tdata,tspeq,1,limit,limit,1);
	for(i=1;i<=pm->ftdim;i++) //unscramble FFT
		for(j=1;j<=pm->ftdim;j++){
			tmp=tdata[1][i][2*j-1]*tdata[1][i][2*j-1];
			tmp+=tdata[1][i][2*j]*tdata[1][i][2*j];
			pm->ppft[i-1][j-1]=sqrt(tmp);
			tmp=tdata[1][limit-i+1][2*j-1]*tdata[1][limit-i+1][2*j-1];
			tmp+=tdata[1][limit-i+1][2*j]*tdata[1][limit-i+1][2*j]; 
			pm->ppft[-i][j-1]=sqrt(tmp);
		}

//	pm->ppft[0][0]=0;	
	free_dmatrix(tspeq,1,1,1,limit*2);
	free_f3tensor(tdata,1,1,1,limit,1,limit);
}


double calcVL(MODEL *pm){//calculation of the cumulant VL (see Binder, p61)
	long i;
	double E1,E2,E4,Etmp;

	E1=0;
	E2=0;
	E4=0;
	for(i=pm->nstartMC+1;i<=pm->nstartMC+pm->nMC;i++){
		E1+=pm->pE[i];
	}
	E1/=pm->nMC;
	for(i=pm->nstartMC+1;i<=pm->nstartMC+pm->nMC;i++){
		Etmp=pm->pE[i]-E1;
		Etmp*=Etmp;
		E2+=Etmp;
		E4+=Etmp*Etmp;
	}
	E2/=pm->nMC;
	E4/=pm->nMC;
	return 1.-E4/3./E2/E2;

}

double calcsigrel(MODEL *pm){
	long i,iMC;
	double E2,E,xi,Exi,Etmp;

	E=0;
	E2=0;
	xi=0;
	Exi=0;
	for(iMC=pm->nstartMC+1,i=0;i<pm->nMC;i++,iMC++){
		Etmp=pm->pE[iMC];
		E+=Etmp;
		Exi+=i*Etmp;
		Etmp*=Etmp;
		E2+=Etmp;
	}
	E/=pm->nMC;
	E2/=pm->nMC;
	E2=sqrt(E2-E*E);// SigmaE
	Exi/=pm->nMC;// Avg(E*i)
	Exi-=E*(pm->nMC-1)/2;//Avg(E*i)-Avg(E)*Avg(i)
	Exi=pm->nMC/(pm->nMC-1)*Exi;//cov(i,E)
	Exi=Exi/E2/sqrt((pm->nMC*pm->nMC-1.)/12);//r(i,E)=cov(i,E)/Sig(E)/Sig(i)
	return sqrt(1.-Exi*Exi);

}

int count(MODEL *pm,int i,int j);

int count(MODEL *pm,int i,int j){//recursive auxilary counting function. It counts the number of neighbors which are of the 
	int ret=1;																			//same kind(either hole or particle).Joa

	if(0==pm->ppaux[i][j]) return 0;//stop counting.Joa
	pm->ppaux[i][j]=0;//stop counting for the new site in the next iteraction.Joa
	if(i>0) ret+=count(pm,i-1,j);
	if(i<pm->dim-1) ret+=count(pm,i+1,j);
	if(j>0) ret+=count(pm,i,j-1);
	if(j<pm->dim-1) ret+=count(pm,i,j+1);
	return ret;
}


int count_per(MODEL *pm,int i,int j){//recursive auxilary counting function periodic boundaries
	int ret=1;

	if(0==pm->ppaux[i][j]) return 0;//stop counting.Joa
	pm->ppaux[i][j]=0;//stop counting for this site in the next iteraction.Joa
	ret+=count(pm,(i-1+pm->dim)%pm->dim,j);// ??? % module operator:
	ret+=count(pm,(i+1+pm->dim)%pm->dim,j);//e1 � (e1 / e2) * e2, where both operands are of integral types. Joa
	ret+=count(pm,i,(j-1+pm->dim)%pm->dim);
	ret+=count(pm,i,(j+1+pm->dim)%pm->dim);
	return ret;
}


void clustcorr( MODEL *pm, int k, int l){		//calculates particle correlations among diferent clusters. Joa
	long i,x,y;										//k and l are centered particle coordinates. 
	double rad1;
	int rad;
	for(i=0;i<pm->npart;i++){
		x=pm->p[i].i-k;
		y=pm->p[i].j-l;
		rad1=sqrt(x*x+y*y);    
		rad=rad1;
		if((-1!=pm->ppaux[pm->p[i].i][pm->p[i].j]) && (rad1<10))
				pm->pc11[x][y]++;		//It goes to initialize(&ret) in file.c. After that inside file.c it is read it and write it each pc11 with hr=bread_dmat(ret.pc11,-ret.dim,ret.dim,-ret.dim,ret.dim,fp)
				//pm->dist[rad]++;		//preparing histogram for number of neighbors at distance rad. Joa
				pm->ncor++;
	}
}


int mark(MODEL *pm,int i,int j){						//recursive auxilary counting function. It marks rim cluster and measures perimeter 
	int ret=1;											//same kind(either hole or particle).Joa
	if((0==pm->ppaux[i][j])||(-1==pm->ppaux[i][j])) {
		-1==pm->ppaux[i][j];
		return 0;//stop counting.Joa
}
	pm->ppaux[i][j]=0;//stop counting for the new site in the next iteraction.Joa
	if(i>0) ret+=count(pm,i-1,j);
	if(i<pm->dim-1) ret+=count(pm,i+1,j);
	if(j>0) ret+=count(pm,i,j-1);
	if(j<pm->dim-1) ret+=count(pm,i,j+1);
	return ret;
}


void genclustcorr(MODEL* pm){//Takes centered particle or hole that's surrended by others. called from Main in main.Rutine introduced by Joa
	long i,j,k;//,k=0;
	long part=1,hole=0;

	j=pm->dim*pm->dim;
	if(pm->npart>j/2.) //When filling > 0.5 we count hole-clusters
	{
		hole=1;
		part=0;
	}
	memset(pm->paux1+1,0,pm->npart*sizeof(*(pm->paux1)));//?? writes on pm->paux1+1,a string of size npart*sizeof(*(pm->paux1).Joa
	for(i=0;i<pm->npart;i++)		//particle condition
		pm->ppaux[pm->p[i].i][pm->p[i].j]=part;
	for(i=pm->npart;i<j;i++)		//hole condition
		pm->ppaux[pm->p[i].i][pm->p[i].j]=hole;
	for(i=0;i<j;i++){				//For all sites
		if(1==pm->ppaux[pm->p[i].i][pm->p[i].j]){		
				k=mark(pm,pm->p[i].i,pm->p[i].j);    
				clustcorr(pm, pm->p[i].i,pm->p[i].j);
		}
	}
}

void count_clusters(MODEL* pm){//counts cluster sizes. called from count_clusters(&mod) in main.Joa
	long i,j,k;//,k=0;
	long part=1,hole=0;

	j=pm->dim*pm->dim;
	if(pm->npart>j/2.) //When filling > 0.5 we count hole-clusters
	{
		hole=1;
		part=0;
	}
	memset(pm->paux1+1,0,pm->npart*sizeof(*(pm->paux1)));//?? writes on pm->paux1+1,a string of size npart*sizeof(*(pm->paux1).Joa
	//memset(pm->pauxl+1,0,pm->npart*sizeof(*(pm->pauxl)));
	for(i=0;i<pm->npart;i++)		//particle condition
		pm->ppaux[pm->p[i].i][pm->p[i].j]=part;
	for(i=pm->npart;i<j;i++)		//hole condition
		pm->ppaux[pm->p[i].i][pm->p[i].j]=hole;
	for(i=0;i<j;i++){				//For all sites
		if(1==pm->ppaux[pm->p[i].i][pm->p[i].j]){	
			switch(pm->pottype)
			{
				default:
					k=count(pm,pm->p[i].i,pm->p[i].j);    //e.g k=9. Joa
					break;
				case 3:
				case 4:
					k=count_per(pm,pm->p[i].i,pm->p[i].j);
					break;
			}
//			k+=j;
			pm->paux1[k]++;//Ex. for i1 and j1 paux1[9], it might happen that i2 and j2 also have paux1[6], but now paux1[6] will increase one. it will point to the next elemt?Joa
			//pm->pauxl[dl]++;
		}
	}
	for(i=1;i<=pm->npart;i++){
		pm->phist[i]+=pm->paux1[i];//??
		pm->phist2[i]+=pm->paux1[i]*(double)pm->paux1[i];
		//pm->phistl[i]+=pm->pauxl[i];
	}
	pm->nClHist++;//For each Montecarlo Step? Joa

}


double mobility(MODEL *pm, int p1x,int p2x,int p1y,int p2y,int h1x,int h2x,int h1y,int h2y){
	double en_h,en_p,delta_e,x_h,dr;
	double xa1,xa2,xa_h,ya1,ya2,d1,xb1,xb2,xb_h,yb1,yb2,d2;
	double tmpexp=0;
//	double count=1;
	double alf=1.0;
//	double conduct2=0;
	double invT=1/pm->T;
	int p1,p2,h1,h2;

		xa1=h1x-p1x;
		xa2=h2x-p2x;
		xa_h=xa1+xa2;
		ya1=h1y-p1y;
		ya2=h2y-p2y;
		xa1*=xa1;
		ya1*=ya1;
		xa2*=xa2;
		ya2*=ya2;
		d1=sqrt(xa1+ya1)+sqrt(xa2+ya2);
		xb1=h1x-p2x;
		xb2=h2x-p1x;
		xb_h=xb1+xb2;
		yb1=h1y-p2y;
		yb2=h2y-p1y;
		xb1*=xb1;
		yb1*=yb1;
		xb2*=xb2;
		yb2*=yb2;
		d2=sqrt(xb1+yb1)+sqrt(xb2+yb2);
		p1=pm->ppaux[p1x][p1y];
		p2=pm->ppaux[p2x][p2y];
		h1=pm->ppaux[h1x][h1y];
		h2=pm->ppaux[h2x][h2y];
		if(d1<=d2){ 
			dr=d1;
			x_h=invT*xa_h;
			swappart(p1,h1,pm);
			swappart(p2,h2,pm);
			en_h=nobipolaronint(h1,h2,pm)+nobipolaronint(h2,h1,pm);
			swappart(p1,h1,pm);
			swappart(p2,h2,pm);
			}	
		else{
			dr=d2;
			x_h=invT*xb_h;
			swappart(p1,h2,pm);
			swappart(p2,h1,pm);
			en_h=nobipolaronint(h1,h2,pm)+nobipolaronint(h2,h1,pm);
			swappart(p1,h2,pm);
			swappart(p2,h1,pm);
		}	
		if(dr<10){;
			en_p=nobipolaronint(p1,p2,pm)+nobipolaronint(p2,p1,pm);
			delta_e=en_h-en_p;
			tmpexp=exp(-alf*dr);
			if(en_h>en_p){
				delta_e=en_h-en_p;
				tmpexp*=exp(-delta_e*invT);
				}
//			count+=tmpexp;
			tmpexp*=x_h;
//			conduct2/=count;
		}
return tmpexp;
}

double  hole_pair(MODEL *pm, int pc1,int pc2,int pc3,int pc4,int kk,int ll){	
	double conduct1=0;
	int count1=0;
//	double tmp=0;
	int ext=11;
	if((kk<pm->dim+pm->dim-1-ext)&(pm->npart<pm->ppaux[kk+1][ll])&(2501!=pm->ppaux[kk+1][ll])){
		conduct1+= mobility(pm,pc1,pc2,pc3,pc4,kk,kk+1,ll,ll);
//		count1+=1;
	}
	if((kk>-pm->dim + ext)&(pm->npart<pm->ppaux[kk-1][ll])&(2501!=pm->ppaux[kk-1][ll])){
		conduct1+= mobility(pm,pc1,pc2,pc3,pc4,kk,kk-1,ll,ll);
//	    count1+=1;
	}
	if((ll<pm->dim+pm->dim-1-ext)&(pm->npart<pm->ppaux[kk][ll+1])&(2501!=pm->ppaux[kk][ll+1])){
		conduct1+= mobility(pm,pc1,pc2,pc3,pc4,kk,kk,ll,ll+1);
//		count1+=1;
	}
	if((ll>-pm->dim + ext)&(pm->npart<pm->ppaux[kk][ll-1])&(2501!=pm->ppaux[kk][ll-1])){
		conduct1+= mobility(pm,pc1,pc2,pc3,pc4,kk,kk,ll,ll-1);
//		count1+=1;
	}
	if (count1=!0) return conduct1/=count1;
	else return conduct1;
}

double bipolaroncond(MODEL *pm, long kk){
	int i,j,k,l,n,m,s,ext;
	double xh1,yh1,xh2,yh2,dr,dr2,r,xp1,yp1,xp2,yp2,dx,dy,dx2,dy2,x_B,ptmp1,ptmp2;
	double x_1,x_2,r1,r2,cond1;
	double en_p,en_h,en_p1,en_p2,en_h1,en_h2,delta_e;
	double alf=2.0;
//	long pp[1],pp[2],pp[3],pp[4];
//	int h[1],h[2],h[3],h[4];
	long pp[5];
	long h[5];
	long p1,h1;
	double tmpexp;
	double invT=1/pm->T;
	double conduct=0;
	int permut=0;
	double cond=0.0;
	double normperm=0.0;
	s=pm->dim*pm->dim;
	ext=11;

	for(i=0;i<pm->npart;i++){
		pm->ppaux[pm->p[i].i][pm->p[i].j]=i;
	}
	for(i=pm->npart;i<s;i++){
			pm->ppaux[pm->p[i].i][pm->p[i].j]=i;
			}

	for(i=0;i<pm->dim-1;i++){
		for(j=0;j<pm->dim-1;j++){
				pm->ppaux[i-pm->dim][j]=pm->ppaux[i][j];
				pm->ppaux[i][j-pm->dim]=pm->ppaux[i][j];
				pm->ppaux[i-pm->dim][j-pm->dim]=pm->ppaux[i][j];
				pm->ppaux[i+pm->dim][j]=pm->ppaux[i][j];
	 			pm->ppaux[i][j+pm->dim]=pm->ppaux[i][j];
				pm->ppaux[i+pm->dim][j+pm->dim]=pm->ppaux[i][j];
				pm->ppaux[i+pm->dim][j-pm->dim]=pm->ppaux[i][j];
				pm->ppaux[i-pm->dim][j+pm->dim]=pm->ppaux[i][j];}}

	for(i=0;i<pm->dim-1;i++){
		for(j=0;j<pm->dim-1;j++){
			if(pm->npart>=pm->ppaux[i][j]&(-2501!=pm->ppaux[i][j])){
				p1=pm->ppaux[i][j];
				xp1=pm->p[p1].i;
				yp1=pm->p[p1].j;
				en_p1=singeint(p1,pm);
				for(m=1;m<=4;m++){
					pp[m]=-1;
				}
				if((i<pm->dim-1)&(pm->npart>=pm->ppaux[i+1][j])&(-2501!=pm->ppaux[i+1][j]))pp[1]=pm->ppaux[i+1][j];
				if((i>0)&(pm->npart>=pm->ppaux[i-1][j])&(-2501!=pm->ppaux[i-1][j])) pp[2]=pm->ppaux[i-1][j];
				if((j<pm->dim-1)&(pm->npart>=pm->ppaux[i][j+1])&(-2501!=pm->ppaux[i][j+1]))pp[3]=pm->ppaux[i][j+1];
				if((j>0)&(pm->npart>=pm->ppaux[i][j-1])&(-2501!=pm->ppaux[i][j-1])) pp[4]=pm->ppaux[i][j-1];
				pm->ppaux[i][j]=-2501;
				for(k=-pm->dim + ext;k<pm->dim+pm->dim-1 - ext;k++){
					for(l=-pm->dim + ext; l<pm->dim+pm->dim-1-ext;l++){
						if(pm->npart<pm->ppaux[k][l]&(2501!=pm->ppaux[k][l])){
							h1=pm->ppaux[k][l];
							en_h1=singeint(h1,pm);
							xh1=pm->p[h1].i;
							yh1=pm->p[h1].j;
							dy=yh1-yp1;
							dx=xh1-xp1;
							for(n=1;n<=4;n++){
								h[n]=-1;}
//							dy=yh-yp;
//							dx=xh-xp;
//							x_B=dx*dx*invT;
//							dr=sqrt(dx*dx+dy*dy);
							if((k<pm->dim+pm->dim-1-ext)&(pm->npart<pm->ppaux[k+1][l])&(2501!=pm->ppaux[k+1][l]))h[1]=pm->ppaux[k+1][l];
							if((k>-pm->dim + ext)&(pm->npart<pm->ppaux[k-1][l])&(2501!=pm->ppaux[k-1][l]))h[2]=pm->ppaux[k-1][l];
							if((l<pm->dim+pm->dim-1-ext)&(pm->npart<pm->ppaux[k][l+1])&(2501!=pm->ppaux[k][l+1])) h[3]=pm->ppaux[k][l+1];
							if((l>-pm->dim + ext)&(pm->npart<pm->ppaux[k][l-1])&(2501!=pm->ppaux[k][l-1])) h[4]=pm->ppaux[k][l-1];
							pm->ppaux[k][l]=2501;
							for(m=1;m<=4;m++){
								for(n=1;n<=4;n++){
									if((-1!=pp[m])&(-1!=h[n])){
										xp2=pm->p[pp[m]].i;
										yp2=pm->p[pp[m]].j;
										xh2=pm->p[h[n]].i;
										yh2=pm->p[h[n]].j;
										dx2=xh2-xp2;
										dy2=yh2-yp2;
										x_1=dx+dx2;
										r1=sqrt(dx*dx+dy*dy)+sqrt(dx2*dx2+dy2*dy2);
										if((r1<10)){
											x_B=x_1*x_1*invT;
											tmpexp=exp(-alf*r1);
											en_p2=singeint(pp[m],pm);
											swappart(p1,h1,pm);
											swappart(pp[m],h[n],pm);
											en_h2=singeint(h[n],pm);
											swappart(p1,h1,pm);
											swappart(pp[m],h[n],pm);
											en_h=en_h1+en_h2;
											en_p=en_p1+en_p2;
											if(en_h>en_p){
												delta_e=en_h-en_p;
												tmpexp*=exp(-delta_e*invT);
												normperm+=tmpexp;
												tmpexp*=x_B;
												cond+=tmpexp;}
										dx=xh1-xp2;
										dy=yh1-yp2;
										dx2=xh2-xp1;
										dy2=yh2-yp1;
										x_2=dx+dx2;
										r2=sqrt(dx*dx+dy*dy)+sqrt(dx2*dx2+dy2*dy2);
											x_B=x_2*x_2*invT;
											tmpexp=exp(-alf*r2);
											ptmp2=p1;
											ptmp1=pp[m];
//											pp[m]=ptmp;
//											en_p=singeint(p1,pm)+singeint(pp[m],pm);
											swappart(ptmp1,h1,pm);
											swappart(ptmp2,h[n],pm);
											en_h=singeint(h1,pm)+singeint(h[n],pm);
											swappart(ptmp1,h1,pm);
											swappart(ptmp2,h[n],pm);
											if(en_h>en_p){
												delta_e=en_h-en_p;
												tmpexp*=exp(-delta_e*invT);
												normperm+=tmpexp;
												tmpexp*=x_B;
												cond+=tmpexp;}
										}
									}
								}	
							}

						}
					}
				}

			}
		}
	}
	if (normperm!=0) cond/=normperm;
	else cond=0;
	return cond;
}

void cleanhist(MODEL *pm){//cleans the cluster-sizes histogram
	long i;
	for(i=1;i<=pm->npart;i++){
		pm->phist[i]=0;
		pm->phist2[i]=0;
		//pm->phistl[i]=0;
	}
//	pm->nClHist=0;
}


void cleanrhohist(MODEL *pm){//cleans the rho record
	long i;
	for(i=0;i<=pm->nMC+pm->nstartMC;i++){
		pm->phist[i]=0;
		//pm->phistl[i]=0;
	}
//	pm->nClHist=0;
}
