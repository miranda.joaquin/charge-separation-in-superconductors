# Charge separation in superconductors 

A study of polaron effects for formation of the charged cluster in cuprates. This mechanism may help to understand their high-temperature superconductivity. The code is written in C++.
